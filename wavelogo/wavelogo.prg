#define PURE_MVC

Import "mod_map";
Import "mod_draw";
Import "mod_video";
Import "mod_key";
Import "mod_proc";
Import "mod_say";
Import "mod_math";
Import "mod_text";
Import "mod_rand";


Const

	screenx=320;
	screeny=240;

End



Global

	int gfx;
	int linea[screeny];
#ifndef PURE_MVC
	float angulos[screeny];
	float offsets[screeny];
#else
	float line_offset[screeny];
#endif
	int delta=32;
	int amplitud=6;
	float velocidad=0.5;
	float velocidad_scroll=1;
	int posicion_scroll;
	int img_ancho;
	int img_alto;
	int wave;
	byte wave_type;
	bool pressed;
	bool debug_info_displayed;

End



Process Main()

Private

	int i;
	int j;

End

Begin

	set_mode(screenx,screeny,32);
	set_fps(60,0);

	//carga la imagen y guarda su ancho y su alto
	gfx=png_load("logo.png");
	img_ancho=graphic_info(0,gfx,G_WIDTH);
	img_alto=graphic_info(0,gfx,G_HEIGHT);

	say("png bit depth=="+graphic_info(0,gfx,G_DEPTH));

	//crea un mapa de un píxel de alto por cada línea de gfx y lo pinta según la línea que sea
	For (j=0; j<img_alto; j++)
		linea[j]=map_new(img_ancho,1,32);

		For (i=0; i<img_ancho; i++)
			map_put_pixel(0,linea[j],i,0,map_get_pixel(0,gfx,i,j));
		End
	End

	//crea tantos procesos como lineas y les pasa su grafico correspondiente
	j=screeny/2-img_alto/2;

	For (i=0; i<img_alto; i++)
		plinea(screenx/2,j,linea[i],i);
		j++;
		say("j=="+j);
	End

	wave=sine_wave();

	FRAME;

	Loop
		If (key(_esc))
			exit();
		End

		simple_controller();

		FRAME;
	End

End



function simple_controller()

Begin

	If (key(_q))
		delta++;
	End
	If (key(_a) AND delta>4)
		delta--;
	End

	If (key(_w))
		amplitud++;
	End
	If (key(_s) AND amplitud>0)
		amplitud--;
	End

	If (key(_e))
		velocidad=velocidad+0.1;
	End
	If (key(_d))
		velocidad=velocidad-0.1;
	End

	If (key(_r))
		velocidad_scroll=velocidad_scroll+0.1;
	End
	If (key(_f))
		velocidad_scroll=velocidad_scroll-0.1;
	End

	If (wave_type!=3 AND key(_v))
		signal(wave,s_kill);
		wave=noise_wave();
	End
	If (wave_type!=2 AND key(_b))
		signal(wave,s_kill);
		wave=saw_wave();
	End
	If (wave_type!=1 AND key(_m))
		signal(wave,s_kill);
		wave=triangle_wave();
	End
	If (wave_type!=0 AND key(_n))
		signal(wave,s_kill);
		wave=sine_wave();
	End

#ifdef PURE_MVC
	If (pressed==0 AND key(_i))
		If (exists(type interleaver))
			signal(type interleaver,s_kill);
		Else
			interleaver();
		End
	End
#endif

	If (pressed==0 AND key(_o))
		If (exists(type logo_scroller))
			signal(type logo_scroller,s_kill);
		Else
			logo_scroller();
		End
	End

	If (pressed==0 AND key(_u))
		toggle_debug_info();
	End
		

	If (pressed==0 AND key(_p))
		scroller_position_reset();
	End

	If (scan_code!=0)
		pressed=1;
	Else
		pressed=0;
	End

End



Function toggle_debug_info()

Begin

	If (debug_info_displayed==0)
		write_var(0,0,0,0,delta);
		write_var(0,0,10,0,amplitud);		
		write_var(0,0,20,0,velocidad);
		write_var(0,0,30,0,velocidad_scroll);
		write_var(0,screenx,0,2,fps);

		debug_info_displayed=1;
	Else
		delete_text(0);
		debug_info_displayed=0;
	End

End



Process plinea(x,y,graph, int n)

Begin

	say("id=="+id+" x=="+x+" y=="+y+" graph=="+graph);

	Loop
#ifndef PURE_MVC
		Switch (wave_type)
			Case 0:
				Repeat
					x=screenx/2+sin(angulos[n])*amplitud;
					FRAME;
				Until (key(_m))
			End

			Case 1:
				Repeat
					x=screenx/2+offsets[n]*amplitud;
					FRAME;
				Until (key(_n))
			End
		End
#else
		x=screenx/2+line_offset[n];
		FRAME;
#endif
	End

End



Process sine_wave()

Private

	float angle_step;
	int i;
#ifdef PURE_MVC
	float angulos[screeny];
#endif

End

Begin

	priority=-1;
	wave_type=0;

	Loop

		angle_step=360000/delta;

		//SETUP
		//hace que cada línea tenga una separación igual a angle_step respecto a la anterior
		//angulos[0]=angulos[img_alto-1]+angle_step;

		For (i=1;i<img_alto;i++)
			angulos[i]=angulos[i-1]+angle_step;

			If (angulos[i]>360000)
				angulos[i]=angulos[i]-360000;
			End

			//say("angulos["+i+"]=="+angulos[i]);
		End

		//ANGLE STEPS
		//desplaza cada línea un valor igual a angle_step*velocidad respecto a su valor anterior
		Repeat
			For (i=0;i<img_alto;i++)
				angulos[i]=angulos[i]+angle_step*velocidad;

				If (angulos[i]>360000)
					angulos[i]=angulos[i]-360000;
				End

				//say("angulos["+i+"]=="+angulos[i]);
			End

#ifdef PURE_MVC
			For(i=0;i<img_alto;i++)
				line_offset[i]=sin(angulos[i])*amplitud;
			End
#endif

			FRAME;
		Until (key(_q) OR key(_a))
	End
	
End



Process triangle_wave()

Private 

	float tri_step;
	int i;
	int direction;
	float linear_a[screeny];
#ifdef PURE_MVC
	float offsets[screeny];
#endif

End

Begin

	priority=-1;
	wave_type=1;

	Loop
		//SET-UP DE TRI-STEP (unidades de movimiento)
		tri_step=delta;
		tri_step=4/tri_step;

		say("door1, tri_step=="+tri_step);

		//GENERACION DE ONDA
		For (i=1; i<img_alto; i++)
			linear_a[i]=linear_a[i-1]+tri_step;

			If (linear_a[i]>=4)
				linear_a[i]=linear_a[i]-4;
			End
		End

		say("door2");

		For (i=0; i<img_alto; i++)
			If (linear_a[i]<2)
				offsets[i]=linear_a[i]-1;
			Else
				offsets[i]=(linear_a[i]-3)*-1;
			End
		End

		say("door3");

		//VIBRACION DE ONDA
		Repeat
			For (i=0; i<img_alto; i++)
				linear_a[i]=linear_a[i]+tri_step*velocidad;

				If (linear_a[i]>=4)
					linear_a[i]=linear_a[i]-4;
				End

				If (linear_a[i]<0)
					linear_a[i]=linear_a[i]+4;
				End
			End

			For (i=0; i<img_alto; i++)
				If (linear_a[i]<2)
					offsets[i]=linear_a[i]-1;
				Else
					offsets[i]=(linear_a[i]-3)*-1;
				End
			End

#ifdef PURE_MVC
			For (i=0; i<img_alto; i++)
				line_offset[i]=offsets[i]*amplitud;
			End
#endif

			FRAME;
		Until (key(_q) OR key(_a))
	End

End



Process saw_wave()

Private

	float tri_step;
	int i;
	float linear_a[screeny];
#ifdef PURE_MVC
	float saw_offsets[screeny];
#endif

End

Begin

	priority=-1;
	wave_type=2;

	Loop
		//SET-UP DE TRI-STEP (de momento probamos con 4/delta)
		tri_step=delta;
		tri_step=4/tri_step;

		//GENERACION DE ONDA
		For (i=1; i<img_alto; i++)
			linear_a[i]=linear_a[i-1]+tri_step;

			If (linear_a[i]>=2)
				linear_a[i]=linear_a[i]-2;
			End
		End

		For (i=0; i<img_alto; i++)
			saw_offsets[i]=linear_a[i]-1;
		End

		//VIBRACION DE ONDA
		Repeat
			For (i=0; i<img_alto; i++)
				linear_a[i]=linear_a[i]+tri_step*velocidad;

				If (linear_a[i]>=2)
					linear_a[i]=linear_a[i]-2;
				End

				If (linear_a[i]<0)
					linear_a[i]=linear_a[i]+2;
				End
			End

			For (i=0; i<img_alto; i++)
				saw_offsets[i]=linear_a[i]-1;
			End

#ifdef PURE_MVC
			For (i=0; i<img_alto; i++)
				line_offset[i]=saw_offsets[i]*amplitud;
			End
#endif

			FRAME;
		Until (key(_q) OR key(_a))
	End

End



Process noise_wave()

Private

	int i;
	float noise_offsets[screeny];
	float decimal_part;

End

Begin

	wave_type=3;
	priority=-1;

	Loop
		For (i=0; i<img_alto; i++)
			decimal_part=rand(0,1000);
			decimal_part=decimal_part/1000;
			say("decimal_part=="+decimal_part);
			noise_offsets[i]=rand(-1,1)+decimal_part;
			//say("noise_offsets["+i+"]=="+noise_offsets[i]);
		End

#ifdef PURE_MVC
		For (i=0; i<img_alto; i++)
			line_offset[i]=noise_offsets[i]*amplitud;
			//say("line_offset["+i+"]=="+line_offset[i]);
		End
#endif

		FRAME;
	End

End
#ifdef PURE_MVC
Process interleaver()

Private

	int i;

End

Begin

	priority=-2;
	say("interleaver creado "+id);

	Loop
		For (i=0; i<img_alto; i=i+2)
			line_offset[i]=line_offset[i]*-1;
		End

		FRAME;
	End

OnExit

	say("interleaver destruido "+id);

End
#endif



Process logo_scroller()

Private

	int i;
	int line_id;
	int avance=1;
	float resta_avance;
	

End

Begin

	Loop
		i=img_alto-1;  //el -1 no se si va pero para borrar si que va!
		               //la funcion get_id(type plinea) me devuelve los procesos del mas nuevo al mas antiguo
		While (line_id=get_id(type plinea))  //es decir, al revés de su creación
			If (i+posicion_scroll<img_alto)
				line_id.graph=linea[i+posicion_scroll];
			Else
				line_id.graph=linea[i-img_alto+posicion_scroll];
			End

			i--;
		End

		avance=velocidad_scroll+resta_avance;
		resta_avance=resta_avance+velocidad_scroll-avance;

		If (velocidad_scroll>=0)
			If (posicion_scroll+avance>img_alto)
				posicion_scroll=posicion_scroll+avance-img_alto;
			Else
				posicion_scroll=posicion_scroll+avance;
			End
		Else
			If (posicion_scroll+avance<0)
				posicion_scroll=posicion_scroll+avance+img_alto;
			Else
				posicion_scroll=posicion_scroll+avance;
			End
		End

		FRAME;
	End

End



Function scroller_position_reset()

Private

	int i;
	int line_id;

End

Begin

	i=img_alto-1;  //si no pongo -1 falta una linea
	               //la funcion get_id(type plinea) me devuelve los procesos del mas nuevo al mas antiguo
	While (line_id=get_id(type plinea))  //es decir, al revés de su creación
		line_id.graph=linea[i];
		i--;
	End

	posicion_scroll=0;

End