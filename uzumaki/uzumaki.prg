#DEFINE DIST_ENABLE

Import "mod_video";
Import "mod_key";
Import "mod_map";
Import "mod_draw";
Import "mod_text";
Import "mod_grproc";
Import "mod_say";
Import "mod_proc";
Import "mod_math";


GLOBAL
	int ball_graph;
	int avance_angulo = 1000;
	int framer = 100;
	int ball_count;
	float distancia = 1.0;
END


PROCESS Main( )

PRIVATE
	int angulo_actual;
END

BEGIN
	set_mode( 320, 240, 32 );
	set_fps( 60, 0 );
	graphics_generation( );
	write_var( 0, 0, 0, 0, angulo_actual );
	write_var( 0, 0, 10, 0, avance_angulo );
	write_var( 0, 0, 20, 0, framer );
	write_var( 0, 0, 30, 0, distancia );
	write_var( 0, 320, 0, 2, ball_count );
	Control( );
	
	LOOP
		IF ( key( _esc ) )
			let_me_alone( );
			BREAK;
		END
		
		Ball( angulo_actual );
		angulo_actual = angulo_actual+avance_angulo;
		
		FRAME;
	END
END


PROCESS Ball( int iangle )

PRIVATE
	float fx;
	float fy;
	float avance_fx;
	float avance_fy;
END

BEGIN
	//resolution = 10;
	ball_count++;
	graph = ball_graph;
	x = 160;
	y = 120;
	fx = x;
	fy = y;
	avance_fx = cos( -iangle );
	avance_fy = sin( -iangle );
	
	FRAME;
	
	LOOP
		IF ( x<0 OR x>320 OR y<0 OR y>240 )
			ball_count--;
			BREAK;
		END

		fx = fx+avance_fx;
		fy = fy+avance_fy;
/*			
#ifdef DIST_ENABLE
		IF ( distancia != 1.0 )
					say( "test" );
			fx = fx+distancia*cos( -iangle );
			fy = fy+distancia*sin( -iangle );
		END
#endif
*/
		
		x = fx;
		y = fy;
		
		FRAME ( framer );
	END
END


PROCESS Control( )

BEGIN
	LOOP
		IF ( key( _q ) )
			avance_angulo = avance_angulo+100;
		END
		IF ( key( _a ) )
			avance_angulo = avance_angulo-100;
		END
	
		IF ( key( _w ) )
			framer++;
		END
		IF ( key( _s ) )
			framer--;
		END
		
		IF ( key( _e ) )
			distancia = distancia+0.01;
		END
		IF ( key( _d ) )
			distancia = distancia-0.01;
		END
		IF ( key( _c ) )
			distancia = 1.0;
		END
	
		FRAME;
	END
END


FUNCTION graphics_generation( )

BEGIN
	ball_graph = map_new( 8, 8, 32 );
	drawing_map( 0, ball_graph );
	drawing_color( rgb( 255, 0, 0 ) );
	draw_fcircle( 4, 4, 3 );
END