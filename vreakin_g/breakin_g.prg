
#define SCREENX 320
#define SCREENY 240

#define DEV_PAD_X 80 //80
#define DEV_PAD_Y 64 //36

#define TOTAL_SCREENX DEV_PAD_X+SCREENX
#define TOTAL_SCREENY DEV_PAD_Y+SCREENY

#define BORDER_RIGHT TOTAL_SCREENX
#define BORDER_DOWN TOTAL_SCREENY
#define BORDER_LEFT DEV_PAD_X
#define BORDER_UP DEV_PAD_Y

#define HSX DEV_PAD_X+(SCREENX/2)
#define HSY DEV_PAD_Y+(SCREENY/2)

#define PAD_HEIGHT 4
#define H_PAD_HEIGHT PAD_HEIGHT/2

#define LEVEL_ARRAY_ELEMENTS 9999

#define PRIORITY_CHECK key( _0 )
#define DISPLAY_X res_x
#define DISPLAY_Y res_y

#define SCALE2
#define FM_GUI
#define BALLMODE 3
//#define NOCLIP


Import "mod_map";
Import "mod_key";
Import "mod_text";
Import "mod_grproc";
Import "mod_proc";
Import "mod_math";
Import "mod_draw";
Import "mod_video";
Import "mod_say";
Import "mod_debug";
Import "mod_string";
Import "mod_file";
Import "mod_dir";
Import "mod_regex";



CONST
	//GAMEPLAY GLOBAL STRUCT DEFAULTS
	_default_ball_j_speed = 4.0;
	_default_ball_max_i_speed = 4.0;
	_default_paddle_max_i_speed = 7.5;
	_default_paddle_acc = 0.7;
	_default_paddle_dec = 1.2;
	_default_paddle_convex_nr = 2.0;
	_default_paddle_size = 28;
	_default_ball_size = 6;
	_default_paddle_type = 3;

	_default_ball_pow = 1;

	//PREDEFINED PRIORITIES
	_root_priority = 100;
	_keyboard_reader_priority = 80;
	_keyboard_filters_priority = 70;
	_control_program_priority = 60;

	_ball_priority = 30;
	_blocks_priority = 25;


	//GRAPH VALUES
	_ball_graph = 259;
	_paddle_graph = 256;
	_dbs_graph = 257;
	_dbs2_graph = 258;
	_pwup_graph = 259;

	//MESSAGE CODES
	_ball_impact_msg = 100;
	_ball_paddle_collision = 101;

	_pow_base_up_msg = 1;
	_pow_base_dwn_msg = 2;
	_pow_mod_value_msg = 3;
	_spd_base_up_msg = 4;
	_spd_base_dwn_msg = 5;
	_spd_mod_more_msg = 6;
	_spd_mod_less_msg = 7;

	_pow_base_up_all_msg = 9;
	_pow_base_dwn_all_msg = 10;
	_pow_mod_value_all_msg = 11;
	_spd_base_up_all_msg = 12;
	_spd_base_dwn_all_msg = 13;
	_spd_mod_more_all_msg = 14;
	_spd_mod_less_all_msg = 15;

	//BALL_POW CONSTANTS (GLOBAL)
	_ballpow_v0 = 1;
	_ballpow_v1 = 2;
	_ballpow_v2 = 3;
	_ballpow_v3 = 4;

	_initial_pow_base = 1;
	_bpow_base_max = 7;
	_bpow_base_min = 1;
	_bpow_mod_duration = 300;

	_spd_base_max = 2;
	_spd_base_min = -2;
	_spd_base_step = 2;
	_spd_mod_step = 2;
	_spd_mod_duration = 300;


	_paddle_j_bottom = 4.0;
	_pw_catch_elements = 8;
	_individuals_elements = 8;
	_allball_elements = 8;


#ifdef NOCLIP
	escala_nc=100;
#endif

END



//
//GLOBAL:
//	int res_x,res_y                 resolución de la ventana
//	int res_x_dyn, res_y_dyn        opción de resolución dinámica en cada eje. Ver init.inc
//	int rotacion                    opción de rotación (<0==software, >0==scren_resolution_orientation)
//	int scale_2x_o                  opción de escalado 2X
//
//	int display_y, display_j        toman el valor de la resolución en el eje que le toque (según la rotación)
//	int camera_offset_i             distancia entre el borde del playfield y el borde de la zona visible
//	int camera_offset_j               (en realidad por ahora solo se usan para dar márgenes de DEV_PAD)
//
//	int h_block_length, int h_block_height       valor de la mitad de dichos elementos para ahorrar división entre 2 cada vez que haya
//	int h_ball_size, int h_pad_length              que usarlos. Su valor se establece cada vez que cambia el original
//
//	int file_version_code           se usa para saber la versión de especificacion de los archivos del editor que se cargan para así 
//	                                  saber como tratarlos correctamente
//
//	int blocksleft      número de bloques existentes
//	int idfpg           identificador del fichero de gráficos
//	int idfpg_rt        identificador del fichero de gráficos que pueden ser vueltos a generar durante la partida
//	int redo_limit      valor de blocksleft tras las últimas colisiones
//	int undo_limit      limite de deshacer (valor igual a blocksleft en el momento de inicio de la partida)
//	int game_state      estado del juego, 0=menus iniciales, 1=on, 2=pausa, 3=actualizar gameplay durante el juego
//
//	byte ball_pos				cuenta de 0 a 7, se usa para mostrar información de varias bolas de forma rudimentaria
//	string last_loaded_file		cadena de texto que guarda la ruta del ultimo archivo cargado por el file_manager
//
//	struct undo_buffer[]            guarda información sobre los bloques que son destruídos, esta información
//	                                  se guarda al desaparecer cada bloque y sirve para las funciones undo y redo,
//	                                  undo_buffer no tiene una representación del mapa actual (todos los bloques)
//	                                  sino que actualiza los datos relevantes antes de cada acceso y los elementos
//	                                  de la tabla se ordenan por orden de destrucción de bloque (por lo que se puede
//	                                  usar blocksleft como índice)
//	struct collision_buffer[2]      struct rellenada por el proceso Ball al chocar contra un bloque.
//		int pos_x, int pos_y            posición del bloque
//		int block_id                    id actual del bloque
//
//	struct level                    contiene los datos del nivel cargado
//		int playfield_i;
//		int playfield_j;
//		int max_blocks_i;
//		int max_blocks_j;
//		int block_length;
//		int block_height;
//		byte block_layout[];            posición de los bloques del nivel
//			
//	struct gameplay                 contiene los datos que rigen la jugabilidad
//		float ball_j_speed;             velocidad vertical de la bola (es constante)
//		float ball_max_i_speed;         velocidad horizontal máxima de la bola
//		float paddle_max_i_speed;       velocidad horizontal máxima de la pala
//		float paddle_acc;               aceleración de la pala
//		float paddle_dec;               deceleración de la pala
//		float paddle_convex_nr;         coeficiente de curvatura de la superfície de la pala
//		int paddle_size;                longitud de la pala
//		int ball_size;                  radio de la bola
//		int paddle_type;                tipo de pala (0=original,1=w_acc,2=w_acc2,3=fast_turn,4=rebound)
//
//

GLOBAL

	struct collision_buffer[2]
		float pos_x;
		float pos_y;
		int block_id;
	end
	
	struct undo_buffer[3200]
		float pos_x;
		float pos_y;
		int redo_id;
		byte block_code;
	end

	int res_x;
	int res_y;
	int res_x_dyn;
	int res_y_dyn;
	int rotacion;
	int scale_2x_o;
	
	int display_i;
	int display_j;
	int camera_offset_i = 0;
	int camera_offset_j = 0;
	
	int h_block_length;
	int h_block_height;
	int h_ballsize;
	int h_pad_length;

	int file_version_code;
	
	int blocksleft;
	int idfpg;
	int idfpg_rt;
	int redo_limit;
	int undo_limit;
	int game_state;  //0=menus iniciales, 1=on, 2=pausa, 3=actualizar gameplay durante el juego

	int ball_pow_values[ 3 ] = _ballpow_v0, _ballpow_v1, _ballpow_v2, _ballpow_v3;

	int paddle_id;

	int g_frame_counter;
	byte ball_pos;

	struct pw_catch[_pw_catch_elements-1]
		int pu_type;
		int pu_value;
	end
	int pw_catch_write_index;
	int pw_catch_read_index;

	struct individuals[_individuals_elements-1]
		int pu_type;
		int pu_value;
	end
	int ind_write_index;
	int ind_read_index;

	struct allball[_allball_elements-1]
		int pu_type;
		int pu_value;
	end
	int allb_write_index;
	int allb_read_index;

	struct gameplay
		float ball_j_speed;
		float ball_max_i_speed;
		float paddle_max_i_speed;
		float paddle_acc;
		float paddle_dec;
		float paddle_convex_nr;
		int paddle_size;
		int ball_size;
		int paddle_type;
	end
	
	struct level
		int playfield_i;
		int playfield_j;
		int max_blocks_i;
		int max_blocks_j;
		int block_length;
		int block_height;
		byte block_layout[LEVEL_ARRAY_ELEMENTS];  //LEVEL_ARRAY_ELEMENTS==9999 se corresponde con bonedit1
	end
	
	string last_loaded_file;

	//struct level      //correspondiente a bonedit0
	//	int playfield_i;
	//	int playfield_j;
	//	byte block_layout[479];
	//end
	//int max_blocks_i = 15;

END



INCLUDE "sys/controllers.inc";
INCLUDE "sys/control_profiles.inc";



LOCAL
	float i_pos;
	float j_pos;
	int msg_type;
	int msg_value;
END



INCLUDE "menu/level_loader.inc";
#ifdef FM_GUI
INCLUDE "menu/level_loader-gui.inc"
#endif
INCLUDE "menu/menu_common.inc";
INCLUDE "menu/init_menu.inc";
INCLUDE "menu/game_settings_menu.inc";
INCLUDE "menu/gameplay_edit.inc";
INCLUDE "menu/newlevel_menu.inc";
INCLUDE "init.inc";

#ifdef BALLMODE
 #if BALLMODE==1
	INCLUDE "gobjects/ball.inc";  //no se incrusta
 #endif
 #if BALLMODE==2
	INCLUDE "gobjects/ball2.inc";  //se incrusta
 #endif
 #if BALLMODE==3
 	INCLUDE "gobjects/ball3.inc";  //solo se incrusta en bloques
 #endif
#endif
//NOTA: Los power ups de la bola están implementados sólo para ball3. ball y ball2 están para hacer comprobaciones y debug, pero el plan es borrarlas.

INCLUDE "gobjects/ball-debug.inc";
INCLUDE "gobjects/blocks.inc";
INCLUDE "gobjects/paddles.inc";
INCLUDE "sys/genfunc.inc";




//
//PROCESS Main()
//
//Inicia el programa, llama a Keyboard_reader() y a Init_menu() y entra en un bucle donde se comprueba si se aprieta la tecla
//de terminar el programa. Si esto sucede, termina los demás procesos y cierra el programa.
//
//


PROCESS Main( )

PRIVATE
	int i;  //debug
END

BEGIN
	say( "main16 START" );

#ifdef SCALE2
	scale_resolution = -1;
#endif

	set_mode( 320, 240, 32 );
	priority = _root_priority;
	
	//DEFAULT DATA
	res_x = 320;
	res_y = 240;
	res_x_dyn = 1;
	res_y_dyn = 1;
	scale_2x_o = 1;

	pw_catch_write_index = 0;
	pw_catch_read_index = 0;
	ind_write_index = 0;
	ind_read_index = 0;
	allb_write_index = 0;
	allb_read_index = 0;
	
	IF ( gameplay.paddle_size==0 )  //poner defaults durante el first run
		gameplay.ball_j_speed = _default_ball_j_speed;
		gameplay.ball_max_i_speed = _default_ball_max_i_speed;
		gameplay.paddle_max_i_speed = _default_paddle_max_i_speed;
		gameplay.paddle_acc = _default_paddle_acc;
		gameplay.paddle_dec =_default_paddle_dec;
		gameplay.paddle_convex_nr = _default_paddle_convex_nr;
		gameplay.paddle_size = _default_paddle_size;
		gameplay.ball_size = _default_ball_size;
		gameplay.paddle_type = _default_paddle_type;
		
		h_ballsize = gameplay.ball_size/2;
		h_pad_length = gameplay.paddle_size/2;
	END

	Keyboard_reader( );
	Init_menu( );
	
	LOOP
		IF ( key( _esc ) )  //SALIR
			exit( );
		END
		IF ( key( _f1 ) )
			DEBUG;
		END

		//debug pw_catch[]
		IF ( key( _l ) )  WHILE ( key( _l ) )	FRAME;	END
			FOR (i=0; i<_pw_catch_elements; i++)
				say( "Main: pw_catch["+i+"].pu_type=="+pw_catch[i].pu_type+" pw_catch["+i+"].pu_value=="+pw_catch[i].pu_value);
			END
			say( "--Main: pw_catch_write_index=="+pw_catch_write_index+" pw_catch_read_index=="+pw_catch_read_index);

			FOR (i=0; i<_individuals_elements; i++)
				say( "Main: individuals["+i+"].pu_type=="+individuals[i].pu_type+" individuals["+i+"].pu_value=="+individuals[i].pu_value);
			END
			say( "--Main: ind_write_index=="+ind_write_index+" ind_read_index=="+ind_read_index);

			FOR (i=0; i<_allball_elements; i++)
				say( "Main: allball["+i+"].pu_type=="+allball[i].pu_type+" allball["+i+"].pu_value=="+allball[i].pu_value);
			END
			say( "--Main: allb_write_index=="+allb_write_index+" allb_read_index=="+allb_read_index);
		END
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type MAIN" );
		END
		 g_frame_counter++;
		FRAME;
		
	END	
END