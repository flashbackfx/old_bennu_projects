Build using the BennuGD compiler:

  bgdc breakin_g.prg

A file with .dcb extension will be created


Run using the BennuGD interpreter:

  bgdi breakin_g.dcb


The 'bonedit' folder also contains a buildable project.