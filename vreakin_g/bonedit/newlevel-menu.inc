CONST
	_num_option_elements = 5;
	_otype_uint_arbitrary = 2;
	_otype_float_arbitrary = 6;
	
	_default_playfield_i = 240;
	_default_playfield_j = 320;
	_default_block_length = 16;
	_default_block_height = 8;
	_default_lower_m_ratio = 0.8;
	
	_nlm_labels_x = 0;
	_nlm_numbers_x = 180;
	_nlm_line_height = 20;
END




//
//	PROCESS New_level_menu( )
//
//	Permite crear un nuevo nivel vacío con los parámetros dados. Para ello muestra en pantalla el nombre de las opciones disponibles
//y su valor. Tomando forma de un cursor, permite elegir cada opción y alterar sus valores. Estas alteraciones se hacen a través
//de un proceso que es llamado por cada opción pasandole parámetros que referencien a dicha opción. Según el tipo de opción el 
//proceso gestor puede ser distinto.
//	New_level_menu también llama al proceso NLM_reactive_data() que se encarga de mostrar en pantalla valores de variables (junto a
//sus etiquetas) que no pueden ser alterados directamente por el usuario pero que cambian al alterar los parámetros que trabaja
//New_level_menu().
// Una vez establecidos los datos, si el usuario decide crear un nuevo nivel, estos datos se copian al struct global level, se pone a
//0 los elemntos de level.block_layout[] y se reinicia el editor con los nuevos datos. Cada vez que el usuario acceda a New_level_menu()
//desde el editor aparecerán datos que coinciden con el nivel actual cargado.
//
//PRIVATE
//	int cursor                                      //opción del menú seleccionada
//	int current_coroutine_id                        //id del proceso gestor de opción actual
//	int option_type[_num_option_elements-1]         //tipo de parámetro y de forma de regulación de cada opción
//	int array_of_pointers[_num_option_elements-1]   //contiene punteros a las variables que modifican cada una de las opciones
//	int reactive_data_pointers[]                    //array con punteros que se pasarán al proceso encargado de mostrar los datos
//	                                                    de información deducida (no modificada por el usuario, pero relativa al valor
//	                                                    de otros parámetros).
//	int pointer global_option                       //puntero elegido entre array_of_pointers que se pasa a los procesos gestores de
//	                                                    opción para modificar el valor de la variable apuntada
//	float pointer f_global_option                   //igual que int pointer global_option pero para trabajar con floats
//	int int_ranges[][1]                             //valores mínimo [0] y máximo [1] admitidos por las opciones tipo int (la primera dimension es para cursor)
// 	float float_ranges[][1]                         //igual que int_ranges[][1] pero para las opciones que usan floats
//	int menu_text_id[]                              //guarda los ids de texto devueltos por write() y write_var() para poder borrarlos
//	struct newlevel                                 //igual que la struct global level pero sin el array int block_layout[]. sirve para
//		int playfield_i, playfield_j                    //configurar los datos del nuevo nivel sin sobreescribir el que esté en ese momento
//		int max_blocks_i, max_blocks_j                  //cargado en el editor. max_blocks_i y max_blocks_j se deducen de los otros 4 
//		int block_length, block_height                  //(y de new_lower_margin_ratio)
//	int new_blockfield_i, new_blockfield_j          //tamaño que pueden ocupar los bloques dentro del playfield, se calcula a partir de
//	                                                    valores del struct newlevel y su resultado se copia a las globales g_blockfield_i
//	                                                    y g_blockfield_j respectivamente
//	float f_new_blockfield_j                        //variable float necesaria para hacer calculos con new_lower_margin_ratio (0.0 a 1.0)
//	float new_lower_margin_ratio                    //cantidad de playfield vacío de posibles bloques reservada a la parte inferior de 
//	                                                    este. es un numero entre 0.0 y 1.0 por el que se multiplica newlevel.playfield_j
//	string option_name[_num_option_elements-1]      //etiquetas de cada una de las opciones
//
//


PROCESS New_level_menu( )

PRIVATE
	int cursor;
	int current_coroutine_id;
	int option_type[ _num_option_elements-1 ];
	int array_of_pointers[ _num_option_elements-1 ];
	int reactive_data_pointers[ 3 ];                  //se pasa como puntero a NLM_reactive_data porque este toca variables de newlevel
	int pointer global_option;
	float pointer f_global_option;
	int int_ranges[ _num_option_elements-1 ][ 1 ];
	float float_ranges[ _num_option_elements-1 ][ 1 ];
	int menu_text_id[ _num_option_elements*2-1 ];
	struct newlevel
		int playfield_i;
		int playfield_j;
		int max_blocks_i;
		int max_blocks_j;
		int block_length;
		int block_height;
	end
	int new_blockfield_i;
	int new_blockfield_j;
	float f_new_blockfield_j;
	float new_lower_margin_ratio;
	string option_name[ _num_option_elements-1 ];
END

BEGIN
	say( "NEW_LEVEL_MENU" );
			
	//DATA FILL	
	option_type[ 0 ] = _otype_uint_arbitrary;
	option_type[ 1 ] = _otype_uint_arbitrary;
	option_type[ 2 ] = _otype_uint_arbitrary;
	option_type[ 3 ] = _otype_uint_arbitrary;
	option_type[ 4 ] = _otype_float_arbitrary;
	
	option_name[ 0 ] = "PLAYFIELD_I";
	option_name[ 1 ] = "PLAYFIELD_J";
	option_name[ 2 ] = "BLOCK_LENGTH";
	option_name[ 3 ] = "BLOCK_HEIGHT";
	option_name[ 4 ] = "LOWER_MARGIN_RATIO";
	
	array_of_pointers[ 0 ] = &newlevel.playfield_i;
	array_of_pointers[ 1 ] = &newlevel.playfield_j;
	array_of_pointers[ 2 ] = &newlevel.block_length;
	array_of_pointers[ 3 ] = &newlevel.block_height;
	array_of_pointers[ 4 ] = &new_lower_margin_ratio;
	
	reactive_data_pointers[ 0 ] = &newlevel.max_blocks_i;
	reactive_data_pointers[ 1 ] = &newlevel.max_blocks_j;
	reactive_data_pointers[ 2 ] = &new_blockfield_i;
	reactive_data_pointers[ 3 ] = &new_blockfield_j;

	int_ranges[ 0 ][ 0 ] = 1; int_ranges[ 0 ][ 1 ] = 640;
	int_ranges[ 1 ][ 0 ] = 1; int_ranges[ 1 ][ 1 ] = 640;
	int_ranges[ 2 ][ 0 ] = 1; int_ranges[ 2 ][ 1 ] = 640;
	int_ranges[ 3 ][ 0 ] = 1; int_ranges[ 3 ][ 1 ] = 640;

	float_ranges[ 4 ][ 0 ] = 0.1; float_ranges[ 4 ][ 1 ] = 1.0;
	
	IF ( level.playfield_i==0 )  //load defaults on first run
		newlevel.playfield_i = _default_playfield_i;
		newlevel.playfield_j = _default_playfield_j;
		newlevel.block_length = _default_block_length;
		newlevel.block_height = _default_block_height;
		new_lower_margin_ratio = _default_lower_m_ratio;
	ELSE
		newlevel.playfield_i = level.playfield_i;
		newlevel.playfield_j = level.playfield_j;
		newlevel.block_length = level.block_length;
		newlevel.block_height = level.block_height;
		
		new_lower_margin_ratio = (g_blockfield_j*1.0)/(newlevel.playfield_j)*1.0;  //calcula uno con el mismo resultado
	END
		
	//DEDUCTED DATA
	newlevel.max_blocks_i = newlevel.playfield_i/newlevel.block_length;
	new_blockfield_i = newlevel.max_blocks_i*newlevel.block_length;

	f_new_blockfield_j = newlevel.playfield_j*new_lower_margin_ratio;
	newlevel.max_blocks_j = f_new_blockfield_j/newlevel.block_height;
	new_blockfield_j = newlevel.max_blocks_j*newlevel.block_height;
	
	say( "New_level_menu: data written" );

	//MENU DRAW
	graph = map_new( 16, 8, 32 );
	map_clear( 0, graph, rgb( 255, 0, 0 ) );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	say( "New_level_menu: map drawn" );
	
	text_z = -4;
	FOR ( cursor = 0 ; cursor<_num_option_elements ; cursor++ )  //cursor es usado como i
		menu_text_id[cursor] = write( 0, _nlm_labels_x, _nlm_line_height*cursor, 0, option_name[ cursor ] );
	END
	
	FOR ( cursor = 0 ; cursor<_num_option_elements ; cursor++ )  //cursor es usado como i
		IF ( array_of_pointers[ cursor ]!=0 )
			IF ( option_type[ cursor ]==_otype_uint_arbitrary )

				global_option = array_of_pointers[ cursor ];
				menu_text_id[(cursor+_num_option_elements)] = write_var( 0, _nlm_numbers_x, _nlm_line_height*cursor, 0, *global_option );

			ELSEIF ( option_type[ cursor ]==_otype_float_arbitrary )  //el ultimo elemento (lower_margin_ratio) es float

				f_global_option = array_of_pointers[ cursor ];
				menu_text_id[(cursor+_num_option_elements)] = write_var( 0, _nlm_numbers_x, _nlm_line_height*cursor, 0, *f_global_option );

			END
		END
	END
	
	NLM_reactive_data( 140, &reactive_data_pointers );
	NLM_editor_cover( newlevel.playfield_i, newlevel.playfield_j );
	
	cursor = 0;
	
	say( "New_level_menu: text drawn" );
	
	x = 160;
	y = _nlm_line_height*cursor;
	z = -4;

	LOOP

		//COROUTINE CALL
		IF ( current_coroutine_id==0 )
			IF ( option_type[ cursor ]==_otype_uint_arbitrary )
				say( "New_level_menu: option "+cursor+" is int arbitrary" );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_int_arbitrary( global_option, &graph, int_ranges[cursor][0], int_ranges[cursor][1] );
				
			ELSEIF ( option_type[ cursor ]==_otype_float_arbitrary )
				say( "New_level_menu: option "+cursor+" is float selector" );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_float_arbitrary( global_option, &graph, float_ranges[cursor][0], float_ranges[cursor][1] );

			ELSE 
				say( "Init_menu: ERROR could not determine option_type, cursor=="+cursor );
			END
		END
		
		FRAME;
		
		//NEW LEVEL
		IF ( editor_status==0 )  //first run
			IF ( KEYNEW )
				say( "New_level_menu: Creating new level," );
				
				level.playfield_i = newlevel.playfield_i;
				level.playfield_j = newlevel.playfield_j;
				level.max_blocks_i = newlevel.max_blocks_i;
				level.max_blocks_j = newlevel.max_blocks_j;
				level.block_length = newlevel.block_length;
				level.block_height = newlevel.block_height;
			
				//g_blockfield_i = new_blockfield_i;  //se establecen en start_editor()
				//g_blockfield_j = new_blockfield_j;
				
				start_editor( );
			
				BREAK;
					
			ELSEIF ( key( _f ) )
				say( "New_level_menu: Calling File_manager" );
				File_manager( 1 );
				BREAK;
			END
		ELSE  //from editor
			IF ( BACKFIRE )
				BREAK;
			END
			IF ( KEYNEW )
				say( "New_level_menu: Creating new level," );
				say( "-current editor contents will be lost, please confirm" );
				Confirmation_prompt( 10 );

				LOOP  //confirmacion
					IF ( FIRE )
						
						level.playfield_i = newlevel.playfield_i;
						level.playfield_j = newlevel.playfield_j;
						level.max_blocks_i = newlevel.max_blocks_i;
						level.max_blocks_j = newlevel.max_blocks_j;
						level.block_length = newlevel.block_length;
						level.block_height = newlevel.block_height;
						
						FOR ( cursor = 0; cursor<sizeof(level.block_layout)/sizeof(level.block_layout[0]) ; cursor++ )
							level.block_layout[ cursor ] = 0;
						END
						
						//g_blockfield_i = new_blockfield_i;  //se establecen en start_editor()
						//g_blockfield_j = new_blockfield_j;
			
						stop_editor( );
						start_editor( );
			
						signal( id, s_kill );
					ELSEIF ( BACKFIRE )
						BREAK;
					END
				
					FRAME;
				END
			END
		END
	
		//OPTION CHANGE
		IF ( KEYUP OR KEYDOWN )
			IF ( current_coroutine_id!=0 )
				say( "--New_level_menu: terminar current_coroutine_id=="+current_coroutine_id );
				signal( current_coroutine_id, s_kill );
				current_coroutine_id = 0;
			END
			
			IF ( KEYUP AND cursor>0 )
				cursor--;
			ELSEIF ( KEYDOWN AND cursor<_num_option_elements-1 )
				cursor++;
			END
			y = _nlm_line_height*cursor;
			
		//REACTIVE DATA UPDATE
		ELSEIF ( KEYLEFT OR KEYRIGHT )
			newlevel.max_blocks_i = newlevel.playfield_i/newlevel.block_length;
			new_blockfield_i = newlevel.max_blocks_i*newlevel.block_length;
		
			f_new_blockfield_j = newlevel.playfield_j*new_lower_margin_ratio;
			newlevel.max_blocks_j = f_new_blockfield_j/newlevel.block_height;
			new_blockfield_j = newlevel.max_blocks_j*newlevel.block_height;
		END
		
	END

ONEXIT
		
	//text deletion
	FOR ( cursor = 0; cursor<_num_option_elements*2; cursor++ )
		IF ( menu_text_id[cursor]!=0 )
			delete_text( menu_text_id[cursor] );
		END
	END
	IF ( current_coroutine_id!=0 )
		signal( current_coroutine_id, s_kill );
		current_coroutine_id = 0;
	END
	IF ( exists( type NLM_reactive_data ) )
		signal( type NLM_reactive_data, s_kill );
	END
	IF ( exists( type NLM_editor_cover ) )
		signal( type NLM_editor_cover, s_kill );
	END
	

	map_unload( 0, graph );
	editor_status = 0;
END




//
//PROCESS Option_int_arbitrary( int pointer target_parameter, int pointer parent_graph, int min_number, int max_number )
//
//	int pointer target_parameter;   //dirección de la variable que va a ser alterada
//	int i;                          //se usa para recuperar el int grabado en la variable apuntada por target_parameter, cambiar su valor
//	                                    y volver a grabarlo en la variable apuntada por target_parameter
//	int pointer parent_graph;       //se usa para alterar el gráfico del cursor del menú principal
//	int min_number, max_number;     //valores mínimo y máximo admitidos por la opción
//
//	Se usan en el caso que la opción seleccionada sea del tipo _otype_uint_arbitrary: el resultado es un int.
//	Option_int_arbitrary() es un proceso que permite seleccionar el valor del int que se le va a pasar a la variable apuntada por 
//target_parameter. int i se utiliza para recorrer el rango de ints y grabar el valor elegido en la variable a modificar.
//	Puede usar dos control profiles distintos, uno para elegir los números uno a uno en cada pulsación y otro que permite avanzar sucesivamente
//mientras se tenga pulsada la tecla. El cambio de modo también cambia el gráfico del cursor en el menú principal. Para esto se usa la 
//dirección recibida como parámetro &parent_graph.
//
//


PROCESS Option_int_arbitrary( int pointer target_parameter, int pointer parent_graph, int min_number, int max_number )

PRIVATE
	int i;
END

BEGIN
	say( "-Option_int_arbitrary: target_parameter=="+target_parameter+" *target_parameter=="+*target_parameter );
	
	//DEFAULT CONTROL PROFILE
	current_control_profile = control_profile( 2 );
	map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
	
	LOOP
		//OPTION 
		IF ( KEYLEFT AND *target_parameter>min_number )  //se usa 1 para evitar divisiones entre 0
			i = *target_parameter;
			i--;							
			*target_parameter = i;
		ELSEIF ( KEYRIGHT AND *target_parameter<max_number )
			i = *target_parameter;
			i++;
			*target_parameter = i;
		END
		
		//SWITCH CONTROL PROFILE
		IF ( FIRE )
			IF ( current_control_profile==2 )	
				map_clear( 0, *parent_graph, rgb( 255, 255, 0 ) );
				current_control_profile = control_profile( 1 );
			ELSEIF ( current_control_profile==1 )											
				map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
				current_control_profile = control_profile( 2 );
			ELSE
				say( "-Option_int_arbitrary: ERROR switching control profiles, current_control_profile=="+current_control_profile );
				RETURN;
			END
		END
		
		FRAME;
	END
END




//
//PROCESS Option_float_arbitrary( float pointer target_parameter, int pointer parent_graph, float min_number, float max_number )
//
//	int pointer target_parameter;  //dirección de la variable que va a ser alterada
//	float i;                       //se usa para recuperar el int grabado en la variable apuntada por target_parameter, cambiar su valor
//	                                   y volver a grabarlo en la variable apuntada por target_parameter
//	float f_increment;             //cantidad que se suma o resta al dato modificado
//	int pointer parent_graph;      //se usa para alterar el gráfico del cursor del menú principal
//	float min_number, max_number;  //valores minimo y máximo aceptados por la opción
//
//	Se usan en el caso que la opción seleccionada sea del tipo _otype_float_arbitrary: el resultado es un float.
//	Option_float_arbitrary() es un proceso que permite seleccionar el valor del float que se le va a pasar a la variable apuntada por 
//target_parameter. int i se utiliza para recorrer el rango de floats y grabar el valor elegido en la variable a modificar.
//	Puede usar dos control profiles distintos, uno para elegir los números uno a uno en cada pulsación y otro que permite avanzar sucesivamente
//mientras se tenga pulsada la tecla. El cambio de modo también cambia el gráfico del cursor en el menú principal. Para esto se usa la 
//dirección recibida como parámetro &parent_graph.
//	Este proceso en contreto recorre un rango de 0.0 a 0.1
//
//


PROCESS Option_float_arbitrary( float pointer target_parameter, int pointer parent_graph, float min_number, float max_number )

PRIVATE
	float i;
	float f_increment = 0.01;
END

BEGIN
	say( "-Option_float_arbitrary: target_parameter=="+target_parameter+" *target_parameter=="+*target_parameter );
	
	//DEFAULT CONTROL PROFILE
	current_control_profile = control_profile( 2 );
	map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
	
	LOOP
		//OPTION 
		IF ( KEYLEFT AND *target_parameter>min_number )
			i = *target_parameter;
			i = i-f_increment;							
			*target_parameter = i;
			IF (*target_parameter<min_number)
				*target_parameter = min_number;
			END
		ELSEIF ( KEYRIGHT AND *target_parameter<max_number )
			i = *target_parameter;
			i = i+f_increment;
			*target_parameter = i;
			IF (*target_parameter>max_number)
				*target_parameter = max_number;
			END
		END
		
		//SWITCH CONTROL PROFILE
		IF ( FIRE )
			IF ( current_control_profile==1 )
				current_control_profile = control_profile( 2 );
				//f_increment = 0.01;
				map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
				//say( "-Option_float_arbitrary: setting f_increment to "+f_increment );

			ELSEIF ( current_control_profile==2 )
				current_control_profile = control_profile( 1 );
				//f_increment = 0.01;
				map_clear( 0, *parent_graph, rgb( 255, 255, 0 ) );
				//say( "Option_float_arbitrary: setting f_increment to "+f_increment );
				
			ELSE
				say( "-Option_float_arbitrary: ERROR switching f_increment, f_increment=="+f_increment );
				RETURN;
			END
		END
		
		FRAME;
	END
END




//
//	PROCESS NLM_reactive_data( int vert_padding, int pointer array_of_pointers )
//
// Proceso que muestra en pantalla datos de variables que el usuario no modifica directamente, pero que su valor depende
//de datos que el usuario sí modifica.
//
//PRIVATE
//	int vert_padding                //Distancia vertical desde el borde superior de la pantalla
//	int pointer array_of_pointers   //Punteros a los datos que ha de imprimir. Los recibe como parámetros (en vez de generarlos
//	                                    él mismo) porque algunos de los datos a mostrar se encuentran en el struct newlevel del
//	                                    proceso New_level_menu() que llama a este
//	int i                           //Usado para iterar en bucles
//	int text_id[]                   //Guarda las ids de texto devueltas por write y write_var para poder borrar los textos
//	int pointer global_option       //Toma valores de array_of_pointers para poder mostrar los datos en cuestion (con write y write_var)
//	string data_labels[]            //Etiquetas de las opciones mostradas
//
//


PROCESS NLM_reactive_data( int vert_padding, int pointer array_of_pointers )

PRIVATE
	int i;
	int text_id[7];
	int pointer global_option;
	//float f_blockfield_j;
	string data_labels[] = "MAX_BLOCKS_I", "MAX_BLOCKS_J", "BLOCKFIELD I", "BLOCKFIELD J";
END

BEGIN
	say( "-NLM_REACTIVE_DATA" );

	FOR ( i = 0; i<4; i++ )
		text_id[ i ] = write( 0, _nlm_labels_x, vert_padding+_nlm_line_height*i, 0, data_labels[i] );
	END
	
	FOR ( i = 0; i<4; i++ )
		global_option = array_of_pointers[i];
		text_id[ i+4 ] = write_var( 0, _nlm_numbers_x, vert_padding+_nlm_line_height*i, 0, *global_option );
	END

	signal( id, S_FREEZE );
		
	FRAME;

ONEXIT
	FOR ( i = 0; i<8 ; i++ )
		IF ( text_id[i]!=0 )
			//say( "NLM_reactive_data: deleting text, text_id["+i+"]=="+text_id[i] );
			delete_text( text_id[i] );
		END
	END
END




//
//	PROCESS NLM_editor_cover( int length_i, int height_j )
//
//	Proceso que sirve para tapar el editor mientras New_level_menu() está activo.
//
//


PROCESS NLM_editor_cover( int length_i, int height_j )

BEGIN
	z = -3;
	graph = map_new( length_i+1, height_j+1, 32 );  //estas dos lineas bastarian para tapar el editor si length_i y height_i fueran
	map_clear( 0, graph, rgb( 0, 0, 255 ) );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	x = 0;
	y = 0;
	
	signal( id, s_freeze );
	FRAME;

ONEXIT
	map_unload( 0, graph );
END	