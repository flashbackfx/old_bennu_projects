//
//	FILEMANAGER-GUI.INC
//
//	Incluye los procesos y funciones necesarias para hacer la interfaz gráfica del File_manager().
//	File_manager() está preparado para funcionar sin GUI. El GUI lee datos públicos de File_manager() y modifica sus
//	contenidos de acuerdo a estos.
//
//



#define TOTAL_GUI_ELEMENTS 16

CONST
	_line1 = 2;
	_line2 = 13;
	_line3 =	63;
	_line4 = 83;
	_line5 = 93;
	_line6 = 153;
	_line7 =	173;
	_line8 = 183;
	_line9 = 193;
	_line10 = 203;
	_line11 = 403;
	_line12 = 413;
	
	_column1 = 3;
	_column2 = 33;
	_column3 = 203;
	_column4 = 403;
	
	_cursor_width = 210;
	_cursor_height = 64;
	_cursor_arrow_width = 8;
	_cursor_arrow_height = 8;
END



//
//	Filemanager_gui( File_manager fm )
//
//Imprime información de File_manager en pantalla. También se encarga de llamar a los procesos Gui_cursor_frame() y
//Confirmation_prompt(), que hacen de cursor y de diálogo de confirmación respectivamente.
//Usa la struct gui_elements[] que contiene información sobre qué información mostrar y en que posición mostrarla.
//Cada vez que File_manager cambia de estado borra todo el texto y vuelve a imprimirlo con los datos actualizados.
//
//	PRIVATE
//	int previous_fm_status     //Guarda el estado de fm.fm_status para saber si este ha cambiado (comparandolos)
//	string returned_string;    //Texto devuelto por la función get_info_string(). Si se usa function string get_info_string()
//	                               como parametro de write(), no se imprime texto, sino un identificador. Por esto usamos
//	                               returned_string, pasandola como puntero a get_info_string() para poder imprimir texto.
//
//	struct gui_elements[]      //Array de structs que dicta que datos se imprimiran en pantalla y en que posiciones
//		int xp, yp;               //Posición del dato.
//		string s_label;           //Datos a imprimir. Algunas son cadenas con un formato especial que han de ser pasadas a 
//		                              get_info_string() para obtener la cadena final.
//
//


PROCESS Filemanager_gui( int loadonly, File_manager  fm  )

PRIVATE
	int frame_width = 640;
	int frame_height = 480;
	int i;
	int previous_fm_status;
	string returned_string;

	struct gui_elements[ TOTAL_GUI_ELEMENTS ]
		int xp;
		int yp;
		int text_id;
		string s_label;
	end
END

BEGIN
	priority = father.priority-1;
	say( "FILEMANAGER_GUI" );
			
	//DRAW WINDOW
	z = -3;
	text_z = z-1;
	graph = map_new( frame_width, frame_height, 32 );
	drawing_map( 0, graph );
	map_clear( 0, graph, rgb( 0, 0, 0, ) );
	drawing_color( rgb( 255, 0, 0 ) );
	draw_box( 0, 0, frame_width, 11 );
	drawing_color( rgb( 255, 255, 255 ) );
	draw_rect( 0, 0, frame_width-1, frame_height-1 );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	
	//DATA FILL
	gui_elements[0].xp = _column1;
	gui_elements[0].yp = _line1;
	gui_elements[0].s_label = "FILE MANAGER";
	
	gui_elements[1].xp = _column1;
	gui_elements[1].yp = _line2;
	gui_elements[1].s_label = "func.cdd.0.CURRENT_DIRECTORY:";
	
	gui_elements[2].xp = _column2;
	gui_elements[2].yp = _line3;
	gui_elements[2].s_label = "DIRECTORIES";
	
	gui_elements[3].xp = _column2;
	gui_elements[3].yp = _line4;
	gui_elements[3].s_label = "func.nam.0. ";
	
	gui_elements[4].xp = _column2;
	gui_elements[4].yp = _line5 ;
	gui_elements[4].s_label = "func.num.0.DIR:";
	
	gui_elements[5].xp = _column4;
	gui_elements[5].yp = _line3;
	gui_elements[5].s_label = "FILES";
	
	gui_elements[6].xp = _column4;
	gui_elements[6].yp = _line4;
	gui_elements[6].s_label = "func.nam.1. ";
	
	gui_elements[7].xp = _column4;
	gui_elements[7].yp = _line5;
	gui_elements[7].s_label = "func.num.1.FILE:";
	
	gui_elements[8].xp = _column3;
	gui_elements[8].yp = _line6;
	gui_elements[8].s_label = "INSTRUCTIONS:";
	
	gui_elements[9].xp = _column2;
	gui_elements[9].yp = _line7;
	gui_elements[9].s_label = "func.ins.1.Press FIRE to enter directory";
	
	gui_elements[10].xp = _column2;
	gui_elements[10].yp = _line8;
	gui_elements[10].s_label = "func.ins.1.Press BACK to go back to editor";
	
	gui_elements[11].xp = _column4;
	gui_elements[11].yp = _line7;
	gui_elements[11].s_label = "func.ins.2.Press L to load from file";
	
	IF (loadonly==0)
		gui_elements[12].xp = _column4;
		gui_elements[12].yp = _line8;
		gui_elements[12].s_label = "func.ins.2.Press S to save to file";
	
		gui_elements[13].xp = _column4;
		gui_elements[13].yp = _line9;
		gui_elements[13].s_label = "func.ins.2.Press N to create a new file";
	END
	
	gui_elements[14].xp = _column4;
	gui_elements[14].yp = _line10;
	gui_elements[14].s_label = "func.ins.2.Press BACK to go back to editor";
	
	gui_elements[15].xp = _column1;
	gui_elements[15].yp = _line11;
	gui_elements[15].s_label = "func.cdd.1.Last saved file: ";
	
	gui_elements[16].xp = _column1;
	gui_elements[16].yp = _line12;
	gui_elements[16].s_label = "func.cdd.2.Last loaded file: ";
	
	Gui_cursor_frame( fm );
	
	//FRAME;
	
	LOOP
		//INFO UPDATE
		IF (previous_fm_status!=fm.fm_status OR KEYUP OR KEYDOWN)
			previous_fm_status = fm.fm_status;
			say( "Filemanager_gui: info update" );

			FOR (i = 0; i=<TOTAL_GUI_ELEMENTS; i++)
				IF (gui_elements[i].text_id!=0)
					delete_text( gui_elements[i].text_id );
					gui_elements[i].text_id = 0;
				END
			END

			FOR (i = 0; i=<TOTAL_GUI_ELEMENTS; i++)
				IF ( find( gui_elements[i].s_label, "func" )!=0)
					gui_elements[i].text_id = write( 0, gui_elements[i].xp, gui_elements[i].yp, 0, gui_elements[i].s_label );
				ELSE
					get_info_string( gui_elements[i].s_label, fm, &returned_string );
					gui_elements[i].text_id = write( 0, gui_elements[i].xp, gui_elements[i].yp, 0, returned_string );
				END
			END
		END
		
		//CONFIRMATION PROMPT
		IF (fm.fm_status==3 OR fm.fm_status==4 OR fm.fm_status==5)
			Confirmation_prompt( fm.fm_status );
			REPEAT
				FRAME;
			UNTIL (FIRE OR BACKFIRE)
		END
		
		//FILE MANAGER EXIT
		IF (fm.fm_status==6)
			FOR (i = 0; i=<TOTAL_GUI_ELEMENTS; i++)
				IF (gui_elements[i].text_id!=0)
					delete_text( gui_elements[i].text_id );
					gui_elements[i].text_id = 0;
				END
			END
			map_unload( 0, graph );
			BREAK;
		END
		
		FRAME;
	END
END



//
//	FUNCTION string get_info_string( string info_code, File_manager fm, string pointer returned_string )
//
//	Esta función permite obtener datos dinámicos en una cadena de texto para que Filemanager_gui() pueda escribirla.
//Datos:
//	string info code;                  //La cadena pasada para tratar. Sigue un patrón determinado.
//	File_manager fm;                   //Los datos públicos de File_manager( )
//	srting pointer returned_string;    //Puntero a la cadena que se usará para escribir el texto final.
//	string returned;                   //Cadena devuelta internamente por la función. Funciona con say() pero no con write().
//
//Formato que siguen las cadenas tratadas por esta función:
// Las cadenas tratadas por esta función empiezan con los caracteres "func." esto hace que Filemagaer_gui() las pueda
//	reconocer y llamar a esta función. A continuación vienen:
//
//	string op;        //3 carácteres a partir de la posición 5. Indican que tipo de operación se ejecutará (que
//	                      forma tendrá la cadena devuelta).
//	int arg1i;        //1 carácter a partir de la posición 9. Permite elegir una rama dentro de una operación tratando
//	                      información numérica.
//	string arg2s;     //Los caracteres a partir de la posición 11. Parte constante del texto.
//
//		Formato de la cadena:    func.ooo.i.s*    (o = op; i = arg1i; s* = arg2s )
//
//


FUNCTION string get_info_string( string info_code, File_manager fm, string pointer returned_string )

PRIVATE
	string returned;
	string op;
	int arg1i;
	string arg2s;
END

BEGIN
	op = substr( info_code, 5, 3 );
	arg1i = atoi( substr( info_code, 9, 1 ) );
	arg2s = substr( info_code, 11 );
	
	IF (op=="cdd")
		IF (arg1i==0)
			returned = arg2s+cd( );
		ELSEIF (arg1i==1)
			returned = arg2s+last_saved_file;
		ELSEIF (arg1i==2)
			returned = arg2s+last_loaded_file;
		END
		
	ELSEIF (op=="nam")
		IF (arg1i==0)
			returned = fm.found_dirs[fm.dirs_cursor];
		ELSE  //==1
			returned = fm.found_files[fm.files_cursor];
		END
		
	ELSEIF (op=="num")
		IF (arg1i==0)
			returned = arg2s+itoa( fm.dirs_cursor+1 )+"/"+itoa( fm.num_of_dirs );
		ELSE  //==1
			IF (fm.num_of_files==0)
				returned = "No files found";
			ELSE
				returned = arg2s+itoa( fm.files_cursor+1 )+"/"+itoa( fm.num_of_files );
			END
		END
		
	ELSEIF (op=="ins")
		IF (arg1i==fm.fm_status)  //buscar manera para que se vea al principio tambien
			returned = arg2s;
		ELSE  //!=fm.fm_status
			returned = "";
		END
	END
	*returned_string = returned;  //si no se hace asi la string no se devuelve bien (sale una id)
	//say( "-get_info_string: returned=="+returned );

	RETURN(returned);  //al recoger esto con la funcion write() se escribe una id (con say() no pasa)
END



//
//	PROCESS Confirmation_prompt( int p_fm_status )
//
//	Muestra una ventana que acompaña la acción de File_manager() de pedir confirmación para las acciones de carga, guardado
//o nuevo archivo. Primero crea un mapa de tamaño promp_width*promp_height.
//Según el tipo de operación dada por p_fm_status crea un gráfico que contiene uno de los textos guardados
//en dialog_texts[]. El carácter "/" en estas cadendas de texto significa que ha de visualizarse con salto de línea. Para 
//conseguirlo se usa write_in_map() para pintar por orden cada línea en el mapa principal.
//
//PRIVATE
//	int prompt_width, prompt_height;       //Tamaño del mapa principal
//	int previous_position, new_position;   //Variables que se usan para pasarle los argumentos necesarios a substr()
//	                                           para que lea cada línea
//	int current_line;                      //Variable que cuenta las líneas para dibujarlas por orden
//	int temp_map_id;                       //Variable que guarda el id del mapa devuelto llamando a write_in_map()
//	string dialog_texts[];                 //Las posibles cadenas a tratar
//
//


PROCESS Confirmation_prompt( int p_fm_status )

PRIVATE
	int prompt_width = 320;
	int prompt_height = 240;
	int previous_position;
	int new_position;
	int current_line;
	int temp_map_id;
	string dialog_texts[] = "LOAD FILE/Current editor data will be replaced/please confirm",
	                        "SAVE FILE/Previous file data will be overwritten/please confirm",
	                        "NEW FILE/A new file will be created/please confirm",
	                        "NEW LEVEL/Current editor data will be replaced/please confirm";   //para New_level_menu()
END

BEGIN
	say( "-CONFIRMATION PROMPT" );

	SWITCH (p_fm_status)
		CASE 3:  //LOAD
			p_fm_status = 0;
		END
		CASE 4:  //SAVE
			p_fm_status = 1;
		END
		CASE 5:  //NEW
			p_fm_status = 2;
		END
		
		CASE 10:  //para New_level_menu( )
			p_fm_status = 3;
		END
	END
	
	z = father.z-2;
	graph = map_new( prompt_width, prompt_height, 32 );
	map_clear( 0, graph, rgb( 0, 0, 0 ) );
	drawing_map( 0, graph );
	drawing_color( rgb( 255, 0, 0 ) );
	draw_box( 0, 0, prompt_width, 12 );
	drawing_color( rgb( 255, 255, 255) );
	draw_rect( 0, 0, prompt_width-1, prompt_height-1 );
	
	
	REPEAT
		new_position = find( dialog_texts[p_fm_status], "/", previous_position );
		IF (new_position!=-1)
			temp_map_id = write_in_map( 0, substr( dialog_texts[p_fm_status], previous_position, new_position-previous_position ), 0 );
		ELSE  //ultima linea
			temp_map_id = write_in_map( 0, substr( dialog_texts[p_fm_status], previous_position ), 0 );
		END
		map_put( 0, graph, temp_map_id, 3, 3+12*current_line );
		map_unload( 0, temp_map_id );
		current_line++;
		previous_position = new_position+1;
		//say( "---Confirmation_prompt: mapa dibujado" );
	UNTIL (new_position==-1)
	
	x = _screenx/2;
	y = _screeny/2;
	
	REPEAT
		FRAME;
	UNTIL (FIRE OR BACKFIRE)
ONEXIT
	map_unload( 0, graph );
END



//
//	PROCESS Gui_cursor_frame( File_manager fm )
//
//	Hace de cursor, cambiando de posición según si se están eligiendo directorios o archivos, y respondiendo
//gráficamente a pulsaciones de FIRE. También crea los procesos que harán de indicadores de los movimientos
//posibles del cursor (y se encarga de detenerlos al cerrar el File Manager). Estos son cuatro procesos que 
//envuelven a Gui_cursor_frame() por cada punto cardinal, cambian de posición relativos a Gui_cursor_frame()
//y responden gráficamente a la posición en la que están (en el caso de los indicadores laterales) o a la 
//posición del cursor en una lista (en el caso de los indicadores de arriba y abajo). Todos responden gráficamente
//a la pulsación de la tecla direccional a cuya direccion corresponden si la opción de movimiento está disponible.
//
//PRIVATE
//	int current_status;       //Variable que se usa para detectar cambios en fm.fm_status
//	int pressed;              //Variable que se usa para cambiar de gráfico con las pulsaciones de tecla
//	int map_ids[4];           //Guarda las ids de los gráficos del cursor y los indicadores de posición
//	int cursor_arrow_ids[3]   //Guarda las ids de los procesos que hacen de indicador de posición
//
//


PROCESS Gui_cursor_frame( File_manager fm )

PRIVATE
	int current_status;
	int pressed;
	int map_ids[4];
	int cursor_arrow_ids[3];
END

BEGIN
	say( "-GUI_CURSOR_FRAME" );
	priority = father.priority;

	//GRAPHICS GENERATION
	//cursor frame main
	map_ids[0] = map_new( _cursor_width, _cursor_height, 32 );
	drawing_map( 0, map_ids[0] );
	drawing_color( rgb( 255, 255, 0 ) );
	draw_rect( 0, 0, _cursor_width-1, _cursor_height-1 );
	
	//cursor frame pressed
	map_ids[1] = map_new( _cursor_width, _cursor_height , 32 );
	drawing_map( 0, map_ids[1] );
	drawing_color( rgb( 255, 0, 0 ) );
	draw_rect( 0, 0, _cursor_width-1, _cursor_height-1 );
	
	//arrow circle OFF
	map_ids[2] = map_new( _cursor_arrow_width, _cursor_arrow_height, 32 );
	
	//arrow circle ON
	map_ids[3] = map_new( _cursor_arrow_width, _cursor_arrow_height, 32 );
	drawing_map( 0, map_ids[3] );
	drawing_color( rgb( 255, 255, 0 ) );
	draw_fcircle( _cursor_arrow_width/2, _cursor_arrow_height/2, (_cursor_arrow_width/2)-1 );
	
	//arrow circle PRESSED
	map_ids[4] = map_new( _cursor_arrow_width, _cursor_arrow_height, 32 );
	drawing_map( 0, map_ids[4] );
	drawing_color( rgb( 255, 0, 0, ) );
	draw_fcircle( _cursor_arrow_width/2, _cursor_arrow_height/2, (_cursor_arrow_width/2)-1 );
	
	graphic_set( 0, map_ids[0], G_X_CENTER, 0 );
	graphic_set( 0, map_ids[1], G_X_CENTER, 0 );

	z = father.z-1;
	y = _line4;
	
	graph = map_ids[0];

	//CURSOR ARROWS
	cursor_arrow_ids[0] = Cursor_arrow_side( y, 1, _cursor_width+10, &map_ids, fm );
	cursor_arrow_ids[1] = Cursor_arrow_side( y, 2, -10, &map_ids, fm );
	cursor_arrow_ids[2] = Cursor_arrow_down( y+10+_cursor_height/2, _cursor_width/2, &map_ids, fm );
	cursor_arrow_ids[3] = Cursor_arrow_up( y-10-_cursor_height/2, _cursor_width/2, &map_ids, fm );
	
	LOOP
		IF (fm.fm_status!=current_status)
			IF (fm.fm_status==6)      //back to editor
				BREAK;
			ELSEIF (fm.fm_status==1)  //dirs picker
				x = _column2-10;
			ELSEIF (fm.fm_status==2)  //files picker
				x = _column4-10;
			END
			current_status = fm.fm_status;
		END
		IF (pressed>0)
			pressed--;
			IF (pressed==0)
				graph = map_ids[0];
			END
		END
		IF (FIRE)
			pressed = 4;
			graph = map_ids[1];
		END
		
		FRAME;
	END
	
	//EXIT
	//hay que terminar los procesos arrow desde aqui porque sino, dada la configuracion de prioridades, estos intentan acceder a 
	//variables publicas de File_manager() cuando este ya ha salido, haciendo que reviente el programa
		
	FOR (pressed = 0 ; pressed<sizeof(cursor_arrow_ids)/sizeof(cursor_arrow_ids[0]) ; pressed++)  //'pressed' se usa como 'i'
		IF (cursor_arrow_ids[pressed]!=0)
			signal( cursor_arrow_ids[pressed], S_KILL );
		END
	END
	FOR (pressed = 0 ; pressed<sizeof(map_ids)/sizeof(map_ids[0]) ; pressed++)  //'pressed' se usa como 'i'
		IF (map_ids[pressed]!=0)
			map_unload( 0, map_ids[pressed] );
		END
	END
END



//
//	PROCESS Cursor_arrow_side( y, int fm_status_match, int padding, int pointer p_map_ids, File_manager fm )
//
//	Proceso que hace de indicador de dirección lateral. Al pulsar una de las teclas laterales. Responden a los cambios y
//la posición del cursor al elegir entre controlar la lista de directorios encontrados o la de archivos de nivel.
//El mismo proceso se utiliza para crear el indicador de cada lado (dandole uno u otro valor a fm_status_match).
//
//PRIVATE
//	int fm_status_match;    //Indica el modo en el que el indicador será visible
//	int padding;            //Desplazamiento respecto a Gui_cursor_frame()
//	int pointer p_map_ids;  //Puntero al array donde se encuentran los ids de los gráficos creados por Gui_cursor_frame()
//	int current_status;     //Se usa para detectar cambios en fm.fm_status
//	int pressed;            //Contador que sirve para indicar que se ha de mostrar el gráfico "pulsado"
//
//


PROCESS Cursor_arrow_side( y, int fm_status_match, int padding, int pointer p_map_ids, File_manager fm )

PRIVATE
	int current_status;
	int pressed;
END

BEGIN
	say( "--CURSOR_ARROW_SIDE, fm_status_match=="+fm_status_match );

	priority = father.priority;
	z = father.z;
	x = father.x+padding;
	
	say( "cas:: fm.fm_status=="+fm.fm_status);

	LOOP
		IF (pressed>0)
			pressed--;
			IF (pressed==0 AND current_status!=fm_status_match)
				graph = p_map_ids[2];                  //OFF
			END
		END
		IF (fm.fm_status!=current_status)
			IF (fm.fm_status==1 OR fm.fm_status==2)
				x = father.x+padding;			
				current_status = fm.fm_status;
				IF (current_status==fm_status_match)
					graph = p_map_ids[3];              //ON
				END
				IF (fm_status_match==1 AND KEYRIGHT)
					pressed = 4;
					graph = p_map_ids[4];              //PRESSED
				ELSEIF (fm_status_match==2 AND KEYLEFT)
					pressed = 4;
					graph = p_map_ids[4];              //PRESSED
				END
			END
		END
		FRAME;
	END
END



//
//	PROCESS Cursor_arrow_down( y, int padding, int pointer p_map_ids, File_manager fm )
//
//	Proceso que hace de indicador de movimiento hacia abajo. Está disponible (gráfico "on") cuando la posición del cursor
//en la lista que se esté tratando no sea la última. Pasa a estado activo (gráfico "pressed") cuando se aprieta la tecla
//de dirección hacia abajo. Al llegar al último elemento de la lista este indicador pasa de activo a no disponible (gráfico
//"off").
//
//PRIVATE
//	int padding;                   //Desplazamiento respecto a Gui_cursor_frame()
// int pointer p_map_ids;          //Puntero al array donde se encuentran los ids de los gráficos creados por Gui_cursor_frame()
//	int current_status;            //Se usa para detectar cambios en fm.fm_status
//	int pressed;                   //Contador que sirve para indicar que se ha de mostrar el gráfico "pulsado"
//	int_pointer p_checked_num;     //Puntero al número máximo de elementos de la lista (de File_manager()) tratada
//	int pointer p_checked_cursor   //Puntero al cursor de posición de lista (de File_manager()) tratada
//	bool on_off;                   //Disponibilidad de dirección
//
//


PROCESS Cursor_arrow_down( y, int padding, int pointer p_map_ids, File_manager fm )

PRIVATE
	int current_status;
	int pressed;
	int pointer p_checked_num;
	int pointer p_checked_cursor;
	bool on_off;
END

BEGIN
	say( "--CURSOR_ARROW_DOWN" );

	priority = father.priority;
	z = father.z;
	x = father.x+padding;
	
	LOOP
		IF (fm.fm_status!=current_status)
			IF (fm.fm_status==1 OR fm.fm_status==2)
				x = father.x+padding;
			END

			current_status = fm.fm_status;
		END

		SWITCH (current_status)
			CASE 1:
				p_checked_cursor = &fm.dirs_cursor;
				p_checked_num = &fm.num_of_dirs;
				
				IF ( *p_checked_cursor<*p_checked_num-1)
					//say( "--Cursor_arrow_down IN CASE 1A: p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
					on_off = 1;
				ELSEIF ( *p_checked_cursor>=*p_checked_num-1);
					//say( "--Cursor_arrow_down IN CASE 1B: p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
					on_off = 0;
				END
				
			END
			CASE 2:
				p_checked_cursor = &fm.files_cursor;
				p_checked_num = &fm.num_of_files;
				
				IF ( *p_checked_cursor<*p_checked_num-1)
					//say( "--Cursor_arrow_down IN CASE 2A: p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
					on_off = 1;
				ELSEIF ( *p_checked_cursor>=*p_checked_num-1);
					//say( "--Cursor_arrow_down IN CASE 2B: p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
					on_off = 0;
				END
				
			END
		END

		//say( "/p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );  //por algun motivo esta linea revienta el programa
		say( "p_checked_cursor=="+p_checked_cursor+" p_checked_num=="+p_checked_num );       //pero esta no...
				
		LOOP
			IF (fm.fm_status==0 OR fm.fm_status==1 OR fm.fm_status==2)
				IF (fm.fm_status!=current_status)
					say( "--cad::inner loop broken!" );
					pressed = 0;
					BREAK;
				END
			END
			IF (pressed>0)
				pressed--;
			END
			IF (pressed==0)
				IF (on_off==0)
					graph = p_map_ids[2];      //OFF
				ELSE
					graph = p_map_ids[3];      //ON
				END
			END
			IF (KEYDOWN and on_off==1)
				pressed = 4;
				graph = p_map_ids[4];          //PRESSED
			END			
			IF (KEYUP OR KEYDOWN)
				IF ( *p_checked_cursor<*p_checked_num-1)
					//say( "--Cursor_arrow_down: pressed=="+pressed+" p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
					on_off = 1;
				ELSEIF ( *p_checked_cursor>=*p_checked_num-1);
					//say( "--Cursor_arrow_down: pressed=="+pressed+" p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
					on_off = 0;
				END
			END

			FRAME;
		END
	END
END



//
//	PROCESS Cursor_arrow_up( y, int padding, int pointer p_map_ids, File_manager fm )
//
//	Proceso que hace de indicador de movimiento hacia arriba. Está disponible (gráfico "on") cuando la posición del cursor
//en la lista que se esté tratando no sea la primera. Pasa a estado activo (gráfico "pressed") cuando se aprieta la tecla
//de dirección hacia arriba. Al llegar al primer elemento de la lista este indicador pasa de activo a no disponible (gráfico
//"off").
//
//PRIVATE
//	int padding;                   //Desplazamiento respecto a Gui_cursor_frame()
// int pointer p_map_ids;          //Puntero al array donde se encuentran los ids de los gráficos creados por Gui_cursor_frame()
//	int current_status;            //Se usa para detectar cambios en fm.fm_status
//	int pressed;                   //Contador que sirve para indicar que se ha de mostrar el gráfico "pulsado"
//	int pointer p_checked_cursor   //Puntero al cursor de posición de lista (de File_manager()) tratada
//	bool on_off;                   //Disponibilidad de dirección
//
//


PROCESS Cursor_arrow_up( y, int padding, int pointer p_map_ids, File_manager fm )

PRIVATE
	int current_status;
	int pressed;
	int pointer p_checked_cursor;
	bool on_off;
END

BEGIN
	say( "-CURSOR_ARROW_UP" );

	priority = father.priority;
	z = father.z;
	x = father.x+padding;
	
	LOOP
		IF (fm.fm_status!=current_status)
			IF (fm.fm_status==1 OR fm.fm_status==2)
				x = father.x+padding;
			END
			current_status = fm.fm_status;
		END

		SWITCH (current_status)
			CASE 1:
				p_checked_cursor = &fm.dirs_cursor;
				
				IF ( *p_checked_cursor>0)
					on_off = 1;
				ELSEIF ( *p_checked_cursor=<0);
					on_off = 0;
				END
				
			END
			CASE 2:
				p_checked_cursor = &fm.files_cursor;
				
				IF ( *p_checked_cursor>0)
					on_off = 1;
				ELSEIF ( *p_checked_cursor=<0);
					on_off = 0;
				END
				
			END
		END
		
		//Al acceder a los datos de los punteros desde aqui pasa el mismo error que con Cursor_arrow_down.
	
		//current_status = fm.fm_status;
		LOOP
			IF (fm.fm_status==0 OR fm.fm_status==1 OR fm.fm_status==2)
				IF (fm.fm_status!=current_status)
					pressed = 0;
					BREAK;
				END
			END
			IF (pressed>0)
				pressed--;
			END
			IF (pressed==0)
				IF (on_off==0)
					graph = p_map_ids[2];     //OFF
				ELSE
					graph = p_map_ids[3];     //ON
				END
			END
			IF (KEYUP and on_off==1)
				pressed = 4;
				graph = p_map_ids[4];         //PRESSED
			END			
			IF (KEYUP OR KEYDOWN)
				IF ( *p_checked_cursor>0)
					on_off = 1;
				ELSEIF ( *p_checked_cursor=<0);
					on_off = 0;
				END
			END

			FRAME;
		END
	END
END	