CONST
	_num_option_elements = 6;
	_num_res_array_elements = 4;
END



//
//	PROCESS Game_settings_menu( )
//
// Permite que establezcamos algunos valores globales del juego en el momento de ejecutarlo. Usa un indice cursor para saber que opción ,de entre 
//un total, tenemos elegida. Usa un array option_type[] con tantas casillas como opciones disponibles, en cada una de las casillas se guarda un 
//valor que representa el tipo de opción que corresponde a la posición de cursor. Según el tipo de la opción se llama a un proceso gestor 
//correspondiente, guardando su id. Cuando el cursor cambia, el proceso gestor actual termina su ejecución y se llama a otro, correspondiente al 
//tipo de la nueva opción apuntada por el cursor.
//
// Al ser llamado escribe los datos necesarios para funcionar e imprime las opciones del menú en la pantalla. También controla un puntero que cambia
//de posición según la opción seleccionada (y de color en casos especiales).
//
//


PROCESS Game_settings_menu( )

PRIVATE
	int i;                                              //debug
	int cursor;                                         //opción seleccionada en el menú
	int current_coroutine_id;                           //id del proceso gestor de opción actual
	int option_type[ _num_option_elements-1 ];          //array que contiene el tipo de opción para cada una de ellas		
	int res_array[ 1 ][ _num_res_array_elements ]       //listas de resoluciones disponibles
	                  = 1, 320, 240, 640, 480,          //  [0][0] y [0][1] quedan reservados
	                    1, 240, 320, 480, 640;          //  para recordar la posicion del cursor lateral
	int array_of_pointers[ _num_option_elements-1 ];
	int pointer global_option;
	int int_ranges[ _num_option_elements-1 ][ 1 ];
	int text_ids[ (_num_option_elements*2)-1 ];
	string option_name[ _num_option_elements-1 ];       //Cadenas de texto a mostrar en el menu
END

BEGIN
	say( "GAME_SETTINGS_MENU" );

	//DATA FILL
	
	option_type[ 0 ] = _otype_uint_arbitrary;  //_otype_uint_choice
	option_type[ 1 ] = _otype_uint_arbitrary;  //_otype_uint_choice
	option_type[ 2 ] = _otype_uint_choice;
	option_type[ 3 ] = _otype_uint_choice;
	option_type[ 4 ] = _otype_uint_arbitrary;  //_otype_uint_choice
	option_type[ 5 ] = _otype_uint_arbitrary;  //_otype_uint_choice
	
	option_name[ 0 ] = "RES X DINAMICA";
	option_name[ 1 ] = "RES Y DINAMICA";
	option_name[ 2 ] = "RESOLUCION X";
	option_name[ 3 ] = "RESOLUCION Y";
	option_name[ 4 ] = "ROTACION";
	option_name[ 5 ] = "SCALE 2X";
	
	array_of_pointers[ 0 ] = &res_x_dyn;
	array_of_pointers[ 1 ] = &res_y_dyn;
	array_of_pointers[ 2 ] = &res_x;
	array_of_pointers[ 3 ] = &res_y;
	array_of_pointers[ 4 ] = &rotacion;
	array_of_pointers[ 5 ] = &scale_2x_o;

	int_ranges[ 0 ][ 0 ] = 0; int_ranges[ 0 ][ 1 ] = 1;
	int_ranges[ 1 ][ 0 ] = 0; int_ranges[ 1 ][ 1 ] = 1;

	int_ranges[ 4 ][ 0 ] = -1; int_ranges[ 4 ][ 1 ] = 1;
	int_ranges[ 5 ][ 0 ] = 0; int_ranges[ 5 ][ 1 ] = 1;
	
	say( "Game_settings_menu: data written" );
	say( "array_of_pointers[0]=="+array_of_pointers[ 0 ] );
	say( "-text_ids elements=="+(_num_option_elements*2) );

	//MENU DRAW
	graph = map_new( 16, 16, 32 );
	map_clear( 0, graph, rgb( 255, 0, 0 ) );
	
	say( "Game_settings_menu: map drawn" );
	
	text_z = 0;
	FOR ( cursor = 0 ; cursor<_num_option_elements ; cursor++ )  //cursor es usado como i
		text_ids[cursor] = write( 0, HSX-30, BORDER_UP+32+20*cursor, 2, option_name[ cursor ] );
		say( "-init_menu_text_ids["+cursor+"]=="+text_ids[cursor] );
		say( "-init_menu_text_ids["+cursor+"]=="+option_name[cursor] );
	END
	
	FOR ( cursor = 0 ; cursor<_num_option_elements ; cursor++ )  //cursor es usado como i
		IF ( array_of_pointers[ cursor ]!=0 )
			global_option = array_of_pointers[ cursor ];
			text_ids[(cursor+_num_option_elements)] = write_var( 0, HSX+10, BORDER_UP+32+20*cursor, 2, *global_option );
			say( "-init_menu_text_ids["+(cursor+6)+"]=="+text_ids[cursor+6] );
			say( "-init_menu_text_ids["+(cursor+6)+"]=="+*global_option );
		END
	END
	cursor = 0;
	
	say( "Game_settings_menu: text drawn" );
			
	x = HSX+32;
	y = BORDER_UP+36+20*cursor;
	
	//VERTICAL MENU
	LOOP

		//COROUTINE CALL
		IF ( current_coroutine_id==0 )
			IF ( option_type[ cursor ]==_otype_uint_choice )
				say( "Init_menu: option "+cursor+" is int choice" );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_int_choice( global_option, &res_array[ cursor-2 ], _num_res_array_elements );  //global_option se pasa sin el &
				say( "&array_of_pointers [ "+cursor+" ] == "+&array_of_pointers[ cursor ]+" array_of_pointers [ "+cursor+" ] == "+array_of_pointers[ cursor ] );
				say( "&global_option == "+&global_option+" global_option == "+global_option );
				
			ELSEIF ( option_type[ cursor ]==_otype_uint_arbitrary )
				say( "Init_menu: option "+cursor+" is int arbitrary" );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_int_arbitrary( global_option, &graph, int_ranges[cursor][0], int_ranges[cursor][1] );
				
			//ELSEIF ( option_type[ cursor ]==_otype_mode_selector )  //Option_mode_selector() queda obsoleto
				say( "Init_menu: option "+cursor+" is mode selector" );
				
				//global_option = array_of_pointers[ cursor ];
				//current_coroutine_id = Option_mode_selector( global_option );
			
			ELSE 
				say( "Init_menu: ERROR could not determine option_type, cursor=="+cursor );
			END
		END
		
		//PRIORITY CHECK
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type INIT_MENU" );
		END
	
	
		FRAME;

		//MENU BACK
		IF ( REWIND_K )  WHILE ( REWIND_K ) FRAME; END
			IF ( current_coroutine_id!=0 )
				signal( current_coroutine_id, s_kill );
				current_coroutine_id = 0;
			END
			BREAK;
		END 
		
		//OPTION CHANGE
		IF ( KEYUP OR KEYDOWN )
			say( "Game_settings_menu: up or down recognised" );
			IF ( current_coroutine_id!=0 )
				say( "--Init_menu: terminar current_coroutine_id=="+current_coroutine_id );
				signal( current_coroutine_id, s_kill );
				current_coroutine_id = 0;
			END
			
			IF ( KEYUP AND cursor>0 )
				cursor--;
			ELSEIF ( KEYDOWN AND cursor<_num_option_elements-1 )
				cursor++;
			END
			y = BORDER_UP+36+20*cursor;
		END
		
		
	END

ONEXIT
	text_clearer( (_num_option_elements*2), &text_ids );
	
	/*
	FOR ( i = 0; i<_num_option_elements*2; i++ )
				say( "-Init_menu: menu_info_clear: init_menu_text_ids["+i+"]=="+init_menu_text_ids[i] );
		IF ( init_menu_text_ids[ i ]!=0 )
			delete_text( init_menu_text_ids[i] );
		END
	END
	*/
	
	map_unload( 0, graph );
	Init_menu( );
	
END