//LEVEL_LOADER-GUI.INC
//Interfaz gráfica opcional para el gestor de archivos. Importada desde Bonedit. Reacciona al uso del proceso File_manager()
//que se encuentra en level_loader.inc leyendo variables públicas de este. Las partes correspondientes al grabado de archivos
//existentes o nuevos ha sido comentada y no se compila.



#define TOTAL_GUI_ELEMENTS 16

CONST
	_line1 = 2;
	_line2 = 13;
	_line3 = 63;
	_line4 = 83;
	_line5 = 93;
	_line6 = 153;
	_line7 = 173;
	_line8 = 183;
	_line9 = 193;
	_line10 = 203;
	_line11 = 403;
	_line12 = 413;
	
	_column1 = 3;
	_column2 = 33;
	_column3 = 203;
	_column4 = 403;
	
	_cursor_width = 210;
	_cursor_height = 64;
	_cursor_arrow_width = 8;
	_cursor_arrow_height = 8;
	
	_frame_width = 640;
	_frame_height = 480;
END



PROCESS Filemanager_gui( File_manager fm )

PRIVATE
	int i;
	int previous_fm_status;
	string returned_string;

	struct gui_elements[ TOTAL_GUI_ELEMENTS ]
		int xp;
		int yp;
		int text_id;
		string s_label;
	end
END

BEGIN
	priority = father.priority-1;
			say( "FILEMANAGER_GUI" );
			
	//DRAW WINDOW
	z = -3;
	text_z = z-1;
	graph = map_new( _frame_width, _frame_height, 32 );
	drawing_map( 0, graph );
	map_clear( 0, graph, rgb( 0, 0, 0, ) );
	drawing_color( rgb( 255, 0, 0 ) );
	draw_box( 0, 0, _frame_width, 11 );
	drawing_color( rgb( 255, 255, 255 ) );
	draw_rect( 0, 0, _frame_width-1, _frame_height-1 );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	
	//DATA FILL
	gui_elements[0].xp = _column1;
	gui_elements[0].yp = _line1;
	gui_elements[0].s_label = "FILE MANAGER";
	
	gui_elements[1].xp = _column1;
	gui_elements[1].yp = _line2;
	gui_elements[1].s_label = "func.cdd.0.CURRENT_DIRECTORY:";
	
	gui_elements[2].xp = _column2;
	gui_elements[2].yp = _line3;
	gui_elements[2].s_label = "DIRECTORIES";
	
	gui_elements[3].xp = _column2;
	gui_elements[3].yp = _line4;
	gui_elements[3].s_label = "func.nam.0. ";
	
	gui_elements[4].xp = _column2;
	gui_elements[4].yp = _line5 ;
	gui_elements[4].s_label = "func.num.0.DIR:";
	
	gui_elements[5].xp = _column4;
	gui_elements[5].yp = _line3;
	gui_elements[5].s_label = "FILES";
	
	gui_elements[6].xp = _column4;
	gui_elements[6].yp = _line4;
	gui_elements[6].s_label = "func.nam.1. ";
	
	gui_elements[7].xp = _column4;
	gui_elements[7].yp = _line5;
	gui_elements[7].s_label = "func.num.1.FILE:";
	
	gui_elements[8].xp = _column3;
	gui_elements[8].yp = _line6;
	gui_elements[8].s_label = "INSTRUCTIONS:";
	
	gui_elements[9].xp = _column2;
	gui_elements[9].yp = _line7;
	gui_elements[9].s_label = "func.ins.1.Press FIRE to enter directory";
	
	gui_elements[10].xp = _column2;
	gui_elements[10].yp = _line8;
	gui_elements[10].s_label = "func.ins.1.Press BACK to go back to editor";
	
	gui_elements[11].xp = _column4;
	gui_elements[11].yp = _line7;
	gui_elements[11].s_label = "func.ins.2.Press L to load from file";
	
	/*
	gui_elements[12].xp = _column4;
	gui_elements[12].yp = _line8;
	gui_elements[12].s_label = "func.ins.2.Press S to save to file";
	
	gui_elements[13].xp = _column4;
	gui_elements[13].yp = _line9;
	gui_elements[13].s_label = "func.ins.2.Press N to create a new file";
	*/
	
	gui_elements[14].xp = _column4;
	gui_elements[14].yp = _line10;
	gui_elements[14].s_label = "func.ins.2.Press BACK to go back to editor";
	
	/*
	gui_elements[15].xp = _column1;
	gui_elements[15].yp = _line11;
	gui_elements[15].s_label = "func.cdd.1.Last saved file: ";
	*/
	
	gui_elements[16].xp = _column1;
	gui_elements[16].yp = _line12;
	gui_elements[16].s_label = "func.cdd.2.Last loaded file: ";
	
#ifdef SCALE2
	//scale_resolution = (_frame_width*10000)+_frame_height;
	scale_resolution = -1;
#endif
	set_mode( _frame_width, _frame_height, 32 );
	Gui_cursor_frame( fm );
	
	//FRAME;
	
	LOOP
		//INFO UPDATE
		IF (previous_fm_status!=fm.fm_status OR KEYUP OR KEYDOWN)
			previous_fm_status = fm.fm_status;
			say( "Filemanager_gui: info update" );
		FOR (i = 0; i=<TOTAL_GUI_ELEMENTS; i++)
			IF (gui_elements[i].text_id!=0)
				delete_text( gui_elements[i].text_id );
				gui_elements[i].text_id = 0;
			END
		END
		FOR (i = 0; i=<TOTAL_GUI_ELEMENTS; i++)
			IF ( find( gui_elements[i].s_label, "func" )!=0)
				gui_elements[i].text_id = write( 0, gui_elements[i].xp, gui_elements[i].yp, 0, gui_elements[i].s_label );
			ELSE
				get_info_string( gui_elements[i].s_label, fm, &returned_string );
				gui_elements[i].text_id = write( 0, gui_elements[i].xp, gui_elements[i].yp, 0, returned_string );
			END
		END
		END
		
		//CONFIRMATION PROMPT
		IF (fm.fm_status==3 OR fm.fm_status==4 OR fm.fm_status==5)
			Confirmation_prompt( fm.fm_status );
			REPEAT
				FRAME;
			UNTIL (FIRE OR BACKFIRE)
		END
		
		//FILE MANAGER EXIT
		IF (fm.fm_status==6)
			FOR (i = 0; i=<TOTAL_GUI_ELEMENTS; i++)
				IF (gui_elements[i].text_id!=0)
					delete_text( gui_elements[i].text_id );
					gui_elements[i].text_id = 0;
				END
			END
			map_unload( 0, graph );
			BREAK;
		END
		
		FRAME;
	END
END



FUNCTION string get_info_string( string info_code, File_manager fm, string pointer returned_string )

PRIVATE
	string returned;
	string op;
	int arg1i;
	string arg2s;
END

BEGIN
	op = substr( info_code, 5, 3 );
	arg1i = atoi( substr( info_code, 9, 1 ) );
	arg2s = substr( info_code, 11 );
	
	IF (op=="cdd")
		IF (arg1i==0)
			returned = arg2s+cd( );
		ELSEIF (arg1i==1)
			returned = arg2s+" last_saved_file";
		ELSEIF (arg1i==2)
			returned = arg2s+last_loaded_file;
		END
		
	ELSEIF (op=="nam")
		IF (arg1i==0)
			returned = fm.found_dirs[fm.dirs_cursor];
		ELSE  //==1
			returned = fm.found_files[fm.files_cursor];
		END
		
	ELSEIF (op=="num")
		IF (arg1i==0)
			returned = arg2s+itoa( fm.dirs_cursor+1 )+"/"+itoa( fm.num_of_dirs );
		ELSE  //==0
			IF (fm.num_of_files==0)
				returned = "No files found";
			ELSE
				returned = arg2s+itoa( fm.files_cursor+1 )+"/"+itoa( fm.num_of_files );
			END
		END
		
	ELSEIF (op=="ins")
		IF (arg1i==fm.fm_status)  //buscar manera para que se vea al principio tambien
			returned = arg2s;
		ELSE  //!=fm.fm_status
			returned = "";
		END
	END
	*returned_string = returned;  //si no se hace asi la string no se devuelve bien (sale una id)
	//say( "-get_info_string: returned=="+returned );
	RETURN(returned);  //al recoger esto con la funcion write() se escribe una id (con say() no pasa)
END



PROCESS Confirmation_prompt( int p_fm_status )

PRIVATE
	int prompt_width = 320;
	int prompt_height = 240;
	int previous_position;
	int new_position;
	int current_line;
	int temp_map_id;
	string dialog_texts[] = "LOAD FILE/Current editor data will be replaced/please confirm",
	                        "SAVE FILE/Previous file data will be overwritten/please confirm",
	                        "NEW FILE/A new file will be created/please confirm";
END

BEGIN
	say( "-CONFIRMATION PROMPT" );
	SWITCH (p_fm_status)
		CASE 3:  //LOAD
			p_fm_status = 0;
		END
		CASE 4:  //SAVE
			p_fm_status = 1;
		END
		CASE 5:  //NEW
			p_fm_status = 2;
		END
	END
	
	z = father.z-2;
	graph = map_new( prompt_width, prompt_height, 32 );
	map_clear( 0, graph, rgb( 0, 0, 0 ) );
	drawing_map( 0, graph );
	drawing_color( rgb( 255, 0, 0 ) );
	draw_box( 0, 0, prompt_width, 12 );
	drawing_color( rgb( 255, 255, 255) );
	draw_rect( 0, 0, prompt_width-1, prompt_height-1 );
	
	
	REPEAT
		new_position = find( dialog_texts[p_fm_status], "/", previous_position );
		IF (new_position!=-1)
			temp_map_id = write_in_map( 0, substr( dialog_texts[p_fm_status], previous_position, new_position-previous_position ), 0 );
		ELSE  //ultima linea
			temp_map_id = write_in_map( 0, substr( dialog_texts[p_fm_status], previous_position ), 0 );
		END
		map_put( 0, graph, temp_map_id, 3, 3+12*current_line );
		map_unload( 0, temp_map_id );
		current_line++;
		previous_position = new_position+1;
	UNTIL (new_position==-1)
	
	x = _frame_width/2;
	y = _frame_height/2;
	
	REPEAT
		FRAME;
	UNTIL (FIRE OR BACKFIRE)
ONEXIT
	map_unload( 0, graph );
END



PROCESS Gui_cursor_frame( File_manager fm )

PRIVATE
	int current_status;
	int pressed;
	int map_ids[4];
	int cursor_arrow_ids[3];
END

BEGIN
	say( "-GUI_CURSOR_FRAME" );
	priority = father.priority;

	//GRAPHICS GENERATION
	//cursor frame main
	map_ids[0] = map_new( _cursor_width, _cursor_height, 32 );
	drawing_map( 0, map_ids[0] );
	drawing_color( rgb( 255, 255, 0 ) );
	draw_rect( 0, 0, _cursor_width-1, _cursor_height-1 );
	//cursor frame pressed
	map_ids[1] = map_new( _cursor_width, _cursor_height , 32 );
	drawing_map( 0, map_ids[1] );
	drawing_color( rgb( 255, 0, 0 ) );
	draw_rect( 0, 0, _cursor_width-1, _cursor_height-1 );
	//arrow circle OFF
	map_ids[2] = map_new( _cursor_arrow_width, _cursor_arrow_height, 32 );
	//arrow circle ON
	map_ids[3] = map_new( _cursor_arrow_width, _cursor_arrow_height, 32 );
	drawing_map( 0, map_ids[3] );
	drawing_color( rgb( 255, 255, 0 ) );
	draw_fcircle( _cursor_arrow_width/2, _cursor_arrow_height/2, (_cursor_arrow_width/2)-1 );
	//arrow circle PRESSED
	map_ids[4] = map_new( _cursor_arrow_width, _cursor_arrow_height, 32 );
	drawing_map( 0, map_ids[4] );
	drawing_color( rgb( 255, 0, 0, ) );
	draw_fcircle( _cursor_arrow_width/2, _cursor_arrow_height/2, (_cursor_arrow_width/2)-1 );
	
	graphic_set( 0, map_ids[0], G_X_CENTER, 0 );
	graphic_set( 0, map_ids[1], G_X_CENTER, 0 );

	z = father.z-1;
	y = _line4;
	
	graph = map_ids[0];

	cursor_arrow_ids[0] = Cursor_arrow_side( y, 1, _cursor_width+10, &map_ids, fm );
	cursor_arrow_ids[1] = Cursor_arrow_side( y, 2, -10, &map_ids, fm );
	cursor_arrow_ids[2] = Cursor_arrow_down( y+10+_cursor_height/2, _cursor_width/2, &map_ids, fm );
	cursor_arrow_ids[3] = Cursor_arrow_up( y-10-_cursor_height/2, _cursor_width/2, &map_ids, fm );
	
	LOOP
		IF (fm.fm_status!=current_status)
			IF (fm.fm_status==6)      //back to editor
				BREAK;
			ELSEIF (fm.fm_status==1)  //dirs picker
				x = _column2-10;
			ELSEIF (fm.fm_status==2)  //files picker
				x = _column4-10;
			END
			current_status = fm.fm_status;
		END
		IF (pressed>0)
			pressed--;
			IF (pressed==0)
				graph = map_ids[0];
			END
		END
		IF (FIRE)
			pressed = 4;
			graph = map_ids[1];
		END
		
		FRAME;
	END
	
	//hay que terminar los procesos arrow desde aqui porque sino, dada la configuracion de prioridades, estos intentan acceder a 
	//variables publicas de File_manager() cuando este ya ha salido, haciendo que reviente el programa
		
	FOR (pressed = 0 ; pressed<sizeof(cursor_arrow_ids)/sizeof(cursor_arrow_ids[0]) ; pressed++)  //'pressed' se usa como 'i'
		IF (cursor_arrow_ids[pressed]!=0)
			signal( cursor_arrow_ids[pressed], S_KILL );
		END
	END
	FOR (pressed = 0 ; pressed<sizeof(map_ids)/sizeof(map_ids[0]) ; pressed++)  //'pressed' se usa como 'i'
		IF (map_ids[pressed]!=0)
			map_unload( 0, map_ids[pressed] );
		END
	END
END



PROCESS Cursor_arrow_side( y, int fm_status_match, int padding, int pointer p_map_ids, File_manager fm )

PRIVATE
	int current_status;
	int pressed;
END

BEGIN
	say( "--CURSOR_ARROW_SIDE, fm_status_match=="+fm_status_match );

	priority = father.priority;
	z = father.z;
	x = father.x+padding;

	LOOP
		IF (pressed>0)
			pressed--;
			IF (pressed==0 AND current_status!=fm_status_match)
					graph = p_map_ids[2];               //OFF
			END
		END
		IF (fm.fm_status!=current_status)
			IF (fm.fm_status==1 OR fm.fm_status==2)
				x = father.x+padding;			
				current_status = fm.fm_status;
				IF (current_status==fm_status_match)
					graph = p_map_ids[3];               //ON
				END
				IF (fm_status_match==1 AND KEYRIGHT)
					pressed = 4;
					graph = p_map_ids[4];               //PRESSED
				ELSEIF (fm_status_match==2 AND KEYLEFT)
					pressed = 4;
					graph = p_map_ids[4];               //PRESSED
				END
			END
		END
		FRAME;
	END
END



PROCESS Cursor_arrow_down( y, int padding, int pointer p_map_ids, File_manager fm )

PRIVATE
	int current_status;
	int pressed;
	int pointer p_checked_num;
	int pointer p_checked_cursor;
	bool on_off;
END

BEGIN
	say( "--CURSOR_ARROW_DOWN" );

	priority = father.priority;
	z = father.z;
	x = father.x+padding;
	
	LOOP
		IF (fm.fm_status!=current_status)
			//IF (fm.fm_status==6)  //el proceso es terminado desde Gui_cursor_frame()
			//	BREAK;              //es así para todos los procesos arrow
			//END
			IF (fm.fm_status==1 OR fm.fm_status==2)
				x = father.x+padding;
			END
			current_status = fm.fm_status;
		END
		SWITCH (current_status)
			CASE 1:
				p_checked_cursor = &fm.dirs_cursor;
				p_checked_num = &fm.num_of_dirs;
				
				IF ( *p_checked_cursor<*p_checked_num-1)
					on_off = 1;
				ELSEIF ( *p_checked_cursor>=*p_checked_num-1);
					on_off = 0;
				END
				
			END
			CASE 2:
				p_checked_cursor = &fm.files_cursor;
				p_checked_num = &fm.num_of_files;
				
				IF ( *p_checked_cursor<*p_checked_num-1)
					on_off = 1;
				ELSEIF ( *p_checked_cursor>=*p_checked_num-1)
					on_off = 0;
				END
				
			END
		END


	//Por alguna razón esta comprobación aquí revienta el programa (al igual que el say de abajo). Pasa al intentar acceder a los datos
	//de los punteros mediante *p_checked_cursor o *p_checked_num. Por alguna razón esto no pasa dentro de los switchs de arriba (por
	//eso los he puesto asi) ni en los IF de abajo. No entiendo por qué pasa esto
		//
		//IF ( *p_checked_cursor<*p_checked_num-1)
		//		say( "--Cursor_arrow_down: p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
		//		on_off = 1;
		//ELSEIF ( *p_checked_cursor>=*p_checked_num-1);
		//		say( "--Cursor_arrow_down: p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );
		//		on_off = 0;
		//END

				//say( "/p_checked_cursor=="+*p_checked_cursor+" p_checked_num=="+*p_checked_num );  //por algun motivo esta linea revienta el programa
				say( "p_checked_cursor=="+p_checked_cursor+" p_checked_num=="+p_checked_num );       //pero esta no...
				
		LOOP
			IF (fm.fm_status==0 OR fm.fm_status==1 OR fm.fm_status==2)
				IF (fm.fm_status!=current_status)
					pressed = 0;
					BREAK;
				END
			END
			IF (pressed>0)
				pressed--;
			END
			IF (pressed==0)
				IF (on_off==0)
					graph = p_map_ids[2];    //OFF
				ELSE
					graph = p_map_ids[3];    //ON
				END
			END
			IF (KEYDOWN and on_off==1)
				pressed = 4;
				graph = p_map_ids[4];        //PRESSED
			END			
			IF (KEYUP OR KEYDOWN)
				IF ( *p_checked_cursor<*p_checked_num-1)
					on_off = 1;
				ELSEIF ( *p_checked_cursor>=*p_checked_num-1);
					on_off = 0;
				END
			END

			FRAME;
		END
	END
END



PROCESS Cursor_arrow_up( y, int padding, int pointer p_map_ids, File_manager fm )

PRIVATE
	int current_status;
	int pressed;
	//int pointer p_checked_num;      //en Cursor_arrow_up() no es necesario porque hacemos la comprobación contra 0
	int pointer p_checked_cursor;
	bool on_off;
END

BEGIN
	say( "-CURSOR_ARROW_UP" );

	priority = father.priority;
	z = father.z;
	x = father.x+padding;
	
	LOOP
		IF (fm.fm_status!=current_status)
			IF (fm.fm_status==1 OR fm.fm_status==2)
				x = father.x+padding;
			END
			current_status = fm.fm_status;
		END
		SWITCH (current_status)
			CASE 1:
				p_checked_cursor = &fm.dirs_cursor;
				
				IF ( *p_checked_cursor>0)
					on_off = 1;
				ELSEIF ( *p_checked_cursor=<0);
					on_off = 0;
				END
				
			END
			CASE 2:
				p_checked_cursor = &fm.files_cursor;
				
				IF ( *p_checked_cursor>0)
					on_off = 1;
				ELSEIF ( *p_checked_cursor=<0);
					on_off = 0;
				END
				
			END
		END
		
	//Al acceder a los datos de los punteros desde aqui pasa el mismo error que con Cursor_arrow_down.
	
		LOOP
			IF (fm.fm_status==0 OR fm.fm_status==1 OR fm.fm_status==2)
				IF (fm.fm_status!=current_status)
					pressed = 0;
					BREAK;
				END
			END
			IF (pressed>0)
				pressed--;
			END
			IF (pressed==0)
				IF (on_off==0)
					graph = p_map_ids[2];  //OFF
				ELSE
					graph = p_map_ids[3];  //ON
				END
			END
			IF (KEYUP and on_off==1)
				pressed = 4;
				graph = p_map_ids[4];      //PRESSED
			END			
			IF (KEYUP OR KEYDOWN)
				IF ( *p_checked_cursor>0)
					on_off = 1;
				ELSEIF ( *p_checked_cursor=<0);
					on_off = 0;
				END
			END

			FRAME;
		END
	END
END	