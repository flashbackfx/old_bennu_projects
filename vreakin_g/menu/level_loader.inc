//LEVEL_LOADER.INC
//Procesos para cargar archivos de nivel.



#define KEYLOAD (key(_l)) 
#define KEYSAVE (key(_s)) 
#define KEYNEW (key(_n))



//
// PROCESS File_manager( )
//
// Proceso que hace de gestor de archivos para poder cargar archivos de nivel. Adaptado de Bonedit, se le han quitado las capacidades
//de grabar archivos (existentes o nuevos). Con la información que muestra por consola se puede usar. Level_loader-gui.inc tiene una
//interfaz gráfica opcional para que el uso de File_manager() sea más comodo. Dicha gui usa las variables públicas de File_manager()
//para funcionar.
//
//PUBLIC
//	int fm_status;          //Estado del file manager
//	int num_of_files;       //Cantidad de archivos encontrados en el directorio actual
//	int num_of_dirs;        //Cantidad de directorios encontrados en el directorio actual
// 	int files_cursor;       //Posición apuntada por el selector en la lista de archivos encontrados
//	int dirs_cursos;        //Posición apuntada por el selector en la lista de directorios encontrados
//	string found_files[];   //Array con los nombres de archivo encontrados en el directorio
//	string found_dirs[];    //Array con los nombres de directorio encontrados en el directorio
//


PROCESS File_manager( )

PUBLIC
	int fm_status;
	int num_of_files;
	int num_of_dirs;
	int files_cursor;
	int dirs_cursor;
	
	string found_files[999];
	string found_dirs[999];
END

PRIVATE
	int i;
	int dir_handle;				
	string filename_result;
	string regex_pattern = "^bglvl[[:digit:]][[:digit:]][[:digit:]].lvl$";
END

BEGIN
	say( "FILE_MANAGER" );
	
	LOOP
		SWITCH ( fm_status )
			CASE 0:  //NEW DIR
				dir_handle = diropen( "*" );
				IF (dir_handle==0)
					say( "File_manager: ERROR could not open directory" );
				ELSE
					FOR (i = 0; i<num_of_files; i++)
						found_files[i] = "";
					END
					FOR (i = 0; i<num_of_dirs; i++)
						found_dirs[i] = "";
					END
					files_cursor = 0;
					dirs_cursor = 0;
					num_of_files = 0;
					num_of_dirs = 0;
					LOOP
						filename_result = dirread( dir_handle );
						IF (filename_result=="")
							BREAK;
						END
						IF (regex( regex_pattern, filename_result )==0)
							found_files[num_of_files] = filename_result;
							num_of_files++;
						END
						IF (fileinfo.directory==1)
							found_dirs[num_of_dirs] = filename_result;
							num_of_dirs++;
						END
					END
					say( "File_manager: new directory lists written" );
					dirclose( dir_handle );

#ifdef FM_GUI					
					IF (!exists( type Filemanager_gui ) )  //el gui se llama aquí sino al principio no muestra bien la informacion
						Filemanager_gui( id );
					END
#endif
					
					FRAME;  //hace que el gui actualice la informacion al cambiar de directorio
					fm_status = 1;  //dirs picker
					//FRAME;
					
				END
			END
			
			CASE 1:  //DIR PICKER
				say( "File_manager: current directory:" );
				say( "-"+cd( ) );
				say( "File_manager: directory selection mode" );
				say( "-"+num_of_dirs+" directories found" );
				say( "-File_manager: selected directory: ("+(dirs_cursor+1)+"/"+num_of_dirs+")" );
				say( "--"+found_dirs[dirs_cursor] );
				FRAME;  //hace que no vuelva al editor cuando le damos a BACKFIRE en el prompt
				LOOP
					IF (KEYDOWN AND dirs_cursor<num_of_dirs-1)
						dirs_cursor++;
						say( "-File_manager: selected directory: ("+(dirs_cursor+1)+"/"+num_of_dirs+")" );
						say( "--"+found_dirs[dirs_cursor] );
						
					END
					IF (KEYUP AND dirs_cursor>0)
						dirs_cursor--;
						say( "-File_manager: selected directory: ("+(dirs_cursor+1)+"/"+num_of_dirs+")" );
						say( "--"+found_dirs[dirs_cursor] );
							
					END
					IF (KEYRIGHT)
						fm_status = 2;  //files picker
						BREAK;
					END
					IF (FIRE)
						chdir( found_dirs[dirs_cursor] );
						fm_status = 0;  //list builder
						BREAK;
					END
					IF (key( _backspace )) WHILE (key( _backspace )) FRAME; END
						fm_status = 6;  //back to editor
						i = 0;          //usado temporalmente para saber si hay que llamar a Init_menu() o Init()
						BREAK;
					END
					
					FRAME;
				END
			END
			
			CASE 2:  //FILE PICKER
				say( "File_manager: current directory:" );
				say( "-"+cd( ) );
				say( "File_manager: file selection mode" );
				FRAME;  //hace que no vuelva al editor cuando le damos a BACKFIRE en el prompt
				IF (num_of_files==0)
					say( "File manager: no files found in current directory" );  //ojo! aunque no haya archivos se tiene que poder crear uno nuevo
				ELSE
					say( "-"+num_of_files+" files found" );
					say( "-File_manager: selected file: ("+(files_cursor+1)+"/"+num_of_files+")" );
					say( "--"+found_files[files_cursor] );	
				END
				LOOP
					IF (KEYDOWN AND files_cursor<num_of_files-1)
						files_cursor++;
						say( "-File_manager: selected file: ("+(files_cursor+1)+"/"+num_of_files+")" );
						say( "--"+found_files[files_cursor] );							
					END
					IF (KEYUP AND files_cursor>0)
						files_cursor--;
						say( "-File_manager: selected file: ("+(files_cursor+1)+"/"+num_of_files+")" );
						say( "--"+found_files[files_cursor] );							
					END
					IF (KEYLEFT)			
						fm_status = 1;  //dirs picker
						BREAK;
					END
					IF (num_of_files>0 AND KEYLOAD)
						fm_status = 3;  //load file
						BREAK;
					END
					IF (key( _backspace )) WHILE (key( _backspace )) FRAME; END
						fm_status = 6;  //back to editor
						i = 0;          //usado temporalmente para saber si hay que llamar a Init_menu() o Init()
						BREAK;
					END					
					
					FRAME;
				END
			END
			
			CASE 3:  //LOAD FILE
				say( "File_manager: LOAD file "+found_files[files_cursor] );
				say( "-current editor data will be replaced, please confirm" );
				LOOP
					IF (FIRE)  //confirm
						//i = load( found_files[files_cursor], level );
						i = level_load( found_files[files_cursor] );

						IF (i==1)
							h_block_length = level.block_length/2;
							h_block_height = level.block_height/2;
							last_loaded_file = cd( )+"/"+(found_files[files_cursor]);
						END
						fm_status = 6;  //back to editor
						say( "-File_manager: LOAD operation i=="+i );
						BREAK;
					END
					IF (key( _backspace )) WHILE (key( _backspace )) FRAME; END  //decline
						fm_status = 2;  //files picker
						BREAK;
					END
					
					FRAME;
				END
			END
			
			CASE 6:  //BACK TO EDITOR
				say( "File_manager: exiting, back to editor" );
				IF (i==1)
					Init( 1 );
				ELSEIF (i==0)
					Init_menu( );
				END
				FRAME;  //impide que el editor registre la pulsacion de tecla
				BREAK;
			END
		END
	END

END



//
// FUNCTION level_load( string level_file )
//
// Función que se encarga de leer los archivos de nivel encontrados por el filemanager, reconocer su versión y rellenar
//el struct global level adecuadamente.
// Se usan los siguientes codigos para file_version_code:
//	101011==Bonedit3
//	101010==Bonedit1
//	240==Bonedit0
//


FUNCTION level_load( string level_file )

PRIVATE
	int file_handle;
	int data_size;
	int i;
END

BEGIN
	file_handle = fopen( level_file, O_READ );
	IF (file_handle==0)
		say( "Could not load file!" );

		RETURN(0);
		
	END
	data_size = fread( file_handle, file_version_code );
	IF (file_version_code>=101010 AND file_version_code<=101012)  //Bonedit1, Bonedit3, Bonedit4
		fseek( file_handle, sizeof( file_version_code ), SEEK_SET );
		data_size = data_size+fread( file_handle, level );
		fclose( file_handle );
		say( "--"+level_file+" file_version_code=="+file_version_code );
		say( "--"+data_size+" bytes read from file "+level_file );
		fclose( file_handle );
		
		RETURN(1);
		
	ELSEIF (file_version_code==240)  //Bonedit0
		fseek( file_handle, 0, SEEK_SET );
		data_size = fread( file_handle, level.playfield_i );
		data_size = data_size+fread( file_handle, level.playfield_j );
		data_size = data_size+fread( file_handle, level.block_layout );
		fclose( file_handle );
		say( "--"+level_file+" file_version_code=="+file_version_code );
		say( "--"+data_size+" bytes read from file "+level_file );
		say( "---Value check, level.playfield_i=="+level.playfield_i+" level.playfield_j=="+level.playfield_j );
		level.max_blocks_i = 15;
		level.max_blocks_j = 32;
		level.block_length = 16;
		level.block_height = 8;

		RETURN(1);
		
	ELSE
		say( "--ERROR file_version_code=="+file_version_code+" is not a valid code" );

		RETURN(0);
		
	END
END