//MENU_COMMON.INC
//Procesos y funciones comunes a la mayoria de menus

#define REWIND_K (key(_backspace))


CONST
	_otype_uint_choice = 1;
	_otype_uint_arbitrary = 2;
	_otype_mode_selector = 3;
	_otype_load_level = 4;
	_otype_gplay_edit = 5;
	_otype_float_arbitrary = 6;
	_load_level_option = 100;
	_gplay_edit_option = 101;
END


//
//PROCESS Option_int_choice( int pointer target_parameter, int pointer res_list, int list_elements )
//
//		int pointer res_list;           //dirección al primer elemento del array donde se encuentran los valores disponibles para la opción elegida.
//		int pointer target_parameter;   //dirección de la variable que va a ser alterada
//		int list_elements;              //cantidad de elementos del array con las opciones. recordar que el 0 guarda el lateral_cursor
//		int lateral_cursor;             //permite seleccionar el elemento del array al que apunta res_list, para que el valor que contenga
//		                                    sea guardado en la variable apuntada por target_parameter
//
// Se usan en el caso que la opción seleccionada sea del tipo _otype_uint_choice: el resultado es un int elegido de entre un conjunto de ellos
//(ordenados en un array).
//
// Option_int_choice() es un proceso que permite seleccionar entre los distintos valores que se encuentran en un array de ints, cuya
//posición 0 (primer elemento) se encuentra en la dirrección de memoria a la que apunta int pointer res_list. 
//int lateral cursor es el valor que indica que elemento del array grabaremos en la variable apuntada por target_parameter.
//
// El primer elemento de cada array se utiliza para guardar la posición última del cursor lateral cuando se estaba operando sobre dicho
//array.
//
//


PROCESS Option_int_choice( int pointer target_parameter, int pointer res_list, int list_elements )

PRIVATE
	int lateral_cursor;
END

BEGIN	
	say( "-Option_int_choice: res_list=="+res_list+" target_parameter=="+target_parameter );
	say( "--/:	*res_list=="+*res_list+" *target_parameter=="+*target_parameter );
			
	//CONTROL PROFILE
	current_control_profile = control_profile( 1 );
	
	//OPTION
	lateral_cursor = *res_list;  //recupera la posicion de lateral_cursor (guardada en res_list[0])
	*target_parameter = res_list[ lateral_cursor ];
	
	LOOP
		IF ( KEYLEFT AND lateral_cursor>1 )
			lateral_cursor--;
			*res_list = lateral_cursor;  //guarda lateral_cursor en res_list[0]
			*target_parameter = res_list[ lateral_cursor ];
		ELSEIF ( KEYRIGHT AND lateral_cursor<list_elements )
			lateral_cursor++;
			*res_list = lateral_cursor;  //res_list[0]
			*target_parameter = res_list[ lateral_cursor ];
		END
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type OPTION_INT_CHOICE" );
		END
		
		FRAME;
	END
END



//
//PROCESS Option_int_arbitrary( int cursor, int pointer parent_graph, int min_number, int max_number )
//
//		int pointer target_parameter;   //dirección de la variable que va a ser alterada
//		int cursor;                     //opción seleccionada en el menu, se utiliza para recibir el dato correspondiente en target_parameter
//		int i;                          //se usa para recuperar el int grabado en la variable apuntada por target_parameter, cambiar su valor
//		                                    y volver a grabarlo en la variable apuntada por target_parameter
//		int min_number, max_number;     //representan los límites del rango que puede recorrer la opción
//		int pointer parent_graph;       //se usa para alterar el gráfico del cursor del menú principal
//
// Se usan en el caso que la opción seleccionada sea del tipo _otype_uint_arbitrary: el resultado es un int.
//
// Option_int_arbitrary() es un proceso que permite seleccionar el valor del int que se le va a pasar a la variable apuntada por 
//target_parameter. 
//int i se utiliza para recorrer el rango de ints y grabar el valor elegido en la variable a modificar.
//
// Puede usar dos control profiles distintos, uno para elegir los números uno a uno en cada pulsación y otro que permite avanzar sucesivamente
//mientras se tenga pulsada la tecla. El cambio de modo también cambia el gráfico del cursor en el menú principal. Para esto se usa la 
//dirección recibida como parámetro &parent_graph.
//
//


PROCESS Option_int_arbitrary( int pointer target_parameter, int pointer parent_graph, int min_number, int max_number )

PRIVATE
	int i;
END

BEGIN
	say( "-Option_int_arbitrary: target_parameter=="+target_parameter+" *target_parameter=="+*target_parameter );
	say( "--:: min_number=="+min_number+" max_number=="+max_number );
	
	//DEFAULT CONTROL PROFILE
	current_control_profile = control_profile( 2 );
	map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
	
	FRAME;
	
	//OPTION 
	LOOP
		IF ( KEYLEFT AND *target_parameter>min_number )
			i = *target_parameter;
			i--;							
			*target_parameter = i;
		ELSEIF ( KEYRIGHT AND *target_parameter<max_number )
			i = *target_parameter;
			i++;
			*target_parameter = i;	
		END
		
		//SWITCH CONTROL PROFILE
		IF ( BACKFIRE )
			IF ( current_control_profile==2 )	
				map_clear( 0, *parent_graph, rgb( 255, 255, 0 ) );
				current_control_profile = control_profile( 1 );
				say( "-Option_int_arbitrary: control_profile( 1 )" );
			ELSEIF ( current_control_profile==1 )											
				map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
				current_control_profile = control_profile( 2 );
				say( "Option_int arbitrary: setting control_profile( 2 )" );
			ELSE
				say( "-Option_int_arbitrary: ERROR switching control profiles, current_control_profile=="+current_control_profile );
				RETURN;
			END
		END
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type OPTION_INT_ARBITRARY" );
		END
		
		FRAME;
	END
ONEXIT
	map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );  //vuelve el icono a rojo en caso que quede amarillo, dado que los otros procesos Option... no cambian el icono
END


//
// Estas lineas formaban parte del bucle principal de option_int_arbitrary y tenian sentido al configurar el tamaño del playfield.
// Habria que reimplementar su funcionalidad en el menu en cuestion.
//


		//USE RES_X OR RES_Y VALUE
		//IF ( key( _x ) )  //deberia ir en el menu en cuestion
		//	WHILE ( key( _x ) )	FRAME;	END			
		//	*target_parameter = res_x;
		//ELSEIF ( key( _y ) )
		//	WHILE ( key( _y ) )	FRAME;	END		
		//	*target_parameter = res_y;
		//END



//
//PROCESS Option_float_arbitrary( float pointer target_parameter, int pointer parent_graph, float min_number, float max_number )
//
//	float pointer target_parameter;     //dirección de la variable que va a ser alterada
//	float i;                            //se usa para recuperar el int grabado en la variable apuntada por target_parameter, cambiar su valor
//	                                        y volver a grabarlo en la variable apuntada por target_parameter
//	float f_increment;                  //cantidad que se suma o resta al dato modificado
//	float min_number, max_number;       //representan los límites del rango que puede recorrer la opción
//	int pointer parent_graph;           //se usa para alterar el gráfico del cursor del menú principal
//
//	Se usan en el caso que la opción seleccionada sea del tipo _otype_float_arbitrary: el resultado es un float.
//	Option_float_arbitrary() es un proceso que permite seleccionar el valor del float que se le va a pasar a la variable apuntada por 
//target_parameter. int i se utiliza para recorrer el rango de floats y grabar el valor elegido en la variable a modificar.
//	Puede usar dos control profiles distintos, uno para elegir los números uno a uno en cada pulsación y otro que permite avanzar sucesivamente
//mientras se tenga pulsada la tecla. El cambio de modo también cambia el gráfico del cursor en el menú principal. Para esto se usa la 
//dirección recibida como parámetro &parent_graph.
//	


PROCESS Option_float_arbitrary( float pointer target_parameter, int pointer parent_graph, float min_number, float max_number )

PRIVATE
	float i;
	float f_increment;
END

BEGIN
	say( "-Option_float_arbitrary: target_parameter=="+target_parameter+" *target_parameter=="+*target_parameter );
	say( "--:: min_number=="+min_number+" max_number=="+max_number );
	
	//DEFAULT CONTROL PROFILE
	current_control_profile = control_profile( 2 );
	f_increment = 0.03;
	map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
	
	FRAME;  //impide arrancar reconociendo FIRE ya pulsada
	
	LOOP

		//OPTION 
		IF ( KEYLEFT )
			i = *target_parameter;
			i = i-f_increment;							
			*target_parameter = i;
			IF ( *target_parameter<min_number )
				*target_parameter = min_number;
			END
		ELSEIF ( KEYRIGHT )		 
			i = *target_parameter;
			i = i+f_increment;
			*target_parameter = i;
			IF ( *target_parameter>max_number )
				*target_parameter = max_number;
			END
		END
		
		//SWITCH CONTROL PROFILE
		IF ( BACKFIRE )
			IF ( current_control_profile==1 )
				current_control_profile = control_profile( 2 );
				f_increment = 0.03;  //atencion! la version de bonedit no cambia f_increment
				map_clear( 0, *parent_graph, rgb( 255, 0, 0 ) );
				say( "-Option_float_arbitrary: setting f_increment to "+f_increment );

			ELSEIF ( current_control_profile==2 )
				current_control_profile = control_profile( 1 );
				f_increment = 0.01;
				map_clear( 0, *parent_graph, rgb( 255, 255, 0 ) );
				say( "Option_float_arbitrary: setting f_increment to "+f_increment );
				
			ELSE
				say( "-Option_float_arbitrary: ERROR switching f_increment, f_increment=="+f_increment );
				RETURN;
			END
		END
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type OPTION_INT_ARBITRARY" );
		END
		
		FRAME;
	END
END



//
//ATENCION!: El proceso Option_mode_selector queda obsoleto con el uso de rangos para los menus. Aquí queda como referencia aunque
//sera borrado en proximas versiones.
//
//PROCESS Option_mode_selector( int cursor, int pointer target_parameter )
//
//	int pointer target_parameter;  //dirección de la variable que va a ser alterada
//	int cursor;                    //opción seleccionada en el menu, se utiliza para recibir el dato correspondiente en target_parameter
//	                                   (en realidad de momento no hace nada puesto que sólo hay una opción de este tipo)
//	int i;                         //se usa para recuperar el int grabado en la variable apuntada por target_parameter, cambiar su valor
//	                                   y volver a grabarlo en la variable apuntada por target_parameter
//
// Se usan en el caso que la opción seleccionada sea del tipo _otype_mode_selector: el resultado es un número en un rango representando una
//opción entre varias.
//
// int i se utiliza para recorrer el rango de ints y grabar el valor elegido en la variable a modificar. Este proceso hace que el rango que 
//se pueda usar sea limitado.
//
// Actualmente solo permite elegir en el rango -1..1. Estas funciones de menu se podrian implementar de manera que se le pasaran como parámetro
//un valor mínimo y uno máximo. Esto haría obsoleto este proceso.
//
//

/*
//-----------------------------------------------------------------------------------DESACTIVADO

PROCESS Option_mode_selector( int pointer target_parameter )

PRIVATE
	int i;
END

BEGIN
	say( "-Option_mode_selector: target_parameter=="+target_parameter+" *target_parameter=="+*target_parameter );
			
	current_control_profile = control_profile( 1 );
	
	LOOP
		IF ( KEYLEFT AND *target_parameter>-1 )      //_min_mode_num (-1)
			i = *target_parameter;
			i--;
			*target_parameter = i;
		ELSEIF ( KEYRIGHT AND *target_parameter<1 )  //_max_mode_num (1)
			i = *target_parameter;
			i++;
			*target_parameter = i;
		END
		
		//SWITCH PLAYFIELD AXIS  //implementar en el menu en cuestión
		//Opcion solo para ROTACION
		//IF ( BACKFIRE )
		//	i = playfield_i;
		//	playfield_i = playfield_j;
		//	playfield_j = i;
		//END
		
		IF( PRIORITY_CHECK )
			say( "PRIORITY=="+priority+" id=="+id+" type OPTION_MODE_SELECTOR" );
		END
		
		FRAME;
	END
END	
//----------------------------------------------------------------------------DESACTIVADO END
*/



//
// PROCESS Menu_cover( z, int length_i, int height_j )
//
// Crea un proceso gráfico rectangular de tamaño length_i*height_j que tapa todo lo que haya detrás de z. La idea es 
//que sirva como fondo para los menus que necesiten uno.
//
//


PROCESS Menu_cover( z, int length_i, int height_j )

BEGIN
	graph = map_new( length_i+1, height_j+1, 32 );  //estas dos lineas bastarian para tapar el editor si length_i y height_i fueran
	map_clear( 0, graph, rgb( 0, 0, 255 ) );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );
	x = 0;
	y = 0;
	
	signal( id, s_freeze );
	FRAME;

ONEXIT
	map_unload( 0, graph );
END	