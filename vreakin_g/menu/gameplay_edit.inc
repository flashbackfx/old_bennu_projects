CONST
	_gem_option_elements = 9;
	
	_gem_labels_x = 0;
	_gem_numbers_x = 180;
	_gem_line_height = 20;
END



//
// PROCESS Gameplay_edit_menu( )
//
// Menu que permite alterar los valores del struct gameplay. Para ello usa un struct newgplay que es igual que gameplay.
//En el momento de ser llamado copia los valores actuales de gameplay en newgplay. Mientras se usa el menu se alteran los
//valores de newgplay. La tecla N graba los valores actuales de newgplay en gameplay. La tecla D graba los valores por 
//defecto en newgplay.
//
// Este menú puede ser llamado desde Init_menu() al arrancar o resetear el programa, o por Control_program() durante la partida.
//Por esto, tiene en cuenta el valor de la variable global game_state. Si se alteran los valores de gameplay (con la tecla N) se
//pasa de game_state==2 (juego en pausa) a game_state==3 (el juego tiene que actualizar elementos de la partida). Al salir del 
//menu se compureba si está en game_state==0 (menus iniciales, fuera de la partida) y si es asi llama a Init_menu().
//
//


PROCESS Gameplay_edit_menu( )

PRIVATE
	int cursor;
	int current_coroutine_id;
	int option_type[ _gem_option_elements-1 ];
	int array_of_pointers[ _gem_option_elements-1 ];
	int pointer global_option;
	float pointer f_global_option;
	int int_ranges[ _gem_option_elements-1 ][ 1 ];
	float float_ranges[ _gem_option_elements-1 ][ 1 ];
	int text_ids[ (_gem_option_elements*2)-1 ];
	struct newgplay
		float ball_j_speed;
		float ball_max_i_speed;
		float paddle_max_i_speed;
		float paddle_acc;
		float paddle_dec;
		float paddle_convex_nr;
		int paddle_size;
		int ball_size;
		int paddle_type;
	end
	string option_name[ _gem_option_elements-1 ];
END

BEGIN
	say( "GAMEPLAY_EDIT_MENU" );
			
	//DATA FILL
		
	option_type[ 0 ] = _otype_float_arbitrary;
	option_type[ 1 ] = _otype_float_arbitrary;
	option_type[ 2 ] = _otype_float_arbitrary;
	option_type[ 3 ] = _otype_float_arbitrary;
	option_type[ 4 ] = _otype_float_arbitrary;
	option_type[ 5 ] = _otype_float_arbitrary;
	option_type[ 6 ] = _otype_uint_arbitrary;
	option_type[ 7 ] = _otype_uint_arbitrary;
	option_type[ 8 ] = _otype_uint_arbitrary;
	
	option_name[ 0 ] = "BALL_J_SPEED";
	option_name[ 1 ] = "BALL_MAX_I_SPEED";
	option_name[ 2 ] = "PADDLE_MAX_I_SPEED";
	option_name[ 3 ] = "PADDLE_ACC";
	option_name[ 4 ] = "PADDLE_DEC";
	option_name[ 5 ] = "PADDLE_CONVEX_NR";
	option_name[ 6 ] = "PADDLE_SIZE";
	option_name[ 7 ] = "BALL_SIZE";
	option_name[ 8 ] = "PADDLE_TYPE";
	
	array_of_pointers[ 0 ] = &newgplay.ball_j_speed;
	array_of_pointers[ 1 ] = &newgplay.ball_max_i_speed;
	array_of_pointers[ 2 ] = &newgplay.paddle_max_i_speed;
	array_of_pointers[ 3 ] = &newgplay.paddle_acc;
	array_of_pointers[ 4 ] = &newgplay.paddle_dec;
	array_of_pointers[ 5 ] = &newgplay.paddle_convex_nr;
	array_of_pointers[ 6 ] = &newgplay.paddle_size;
	array_of_pointers[ 7 ] = &newgplay.ball_size;
	array_of_pointers[ 8 ] = &newgplay.paddle_type;

	float_ranges[ 0 ][ 0 ] = 0.01; float_ranges[ 0 ][ 1 ] = 50.0;
	float_ranges[ 1 ][ 0 ] = 0.01; float_ranges[ 1 ][ 1 ] = 50.0;
	float_ranges[ 2 ][ 0 ] = 0.01; float_ranges[ 2 ][ 1 ] = 50.0;
	float_ranges[ 3 ][ 0 ] = 0.01; float_ranges[ 3 ][ 1 ] = 50.0;
	float_ranges[ 4 ][ 0 ] = 0.01; float_ranges[ 4 ][ 1 ] = 50.0;
	float_ranges[ 5 ][ 0 ] = 0.01; float_ranges[ 5 ][ 1 ] = 50.0;

	int_ranges[ 6 ][ 0 ] = 1; int_ranges[ 6 ][ 1 ] = 640;
	int_ranges[ 7 ][ 0 ] = 1; int_ranges[ 7 ][ 1 ] = 200;
	int_ranges[ 8 ][ 0 ] = 0; int_ranges[ 8 ][ 1 ] = 4;  //_paddle_types

	newgplay.ball_j_speed = gameplay.ball_j_speed;
	newgplay.ball_max_i_speed = gameplay.ball_max_i_speed;
	newgplay.paddle_max_i_speed = gameplay.paddle_max_i_speed;
	newgplay.paddle_acc = gameplay.paddle_acc;
	newgplay.paddle_dec = gameplay.paddle_dec;
	newgplay.paddle_convex_nr = gameplay.paddle_convex_nr;
	newgplay.paddle_size = gameplay.paddle_size;
	newgplay.ball_size = gameplay.ball_size;
	newgplay.paddle_type = gameplay.paddle_type;
			
	say( "Gameplay_edit_menu: data filled" );


	//MENU DRAW
	graph = map_new( 16, 8, 32 );
	map_clear( 0, graph, rgb( 255, 0, 0 ) );
	graphic_set( 0, graph, G_X_CENTER, 0 );
	graphic_set( 0, graph, G_Y_CENTER, 0 );

	say( "Gameplay_edit_menu: map drawn" );


	text_z = -4;
	FOR ( cursor = 0 ; cursor<_gem_option_elements ; cursor++ )  //cursor es usado como i
		text_ids[cursor] = write( 0, _gem_labels_x, _gem_line_height*cursor, 0, option_name[ cursor ] );
		//say( "-text_ids["+cursor+"]=="+text_ids[cursor] );
		//say( "-text_ids["+cursor+"]=="+option_name[cursor] );
	END
	
	FOR ( cursor = 0 ; cursor<_gem_option_elements ; cursor++ )  //cursor es usado como i

		IF ( option_type[ cursor ]==_otype_uint_arbitrary )

			global_option = array_of_pointers[ cursor ];
			text_ids[(cursor+_gem_option_elements)] = write_var( 0, _gem_numbers_x, _gem_line_height*cursor, 0, *global_option );
				
			//say( "-text_ids["+(cursor+_gem_option_elements)+"]=="+text_ids[cursor+_gem_option_elements] );
			//say( "-text_ids["+(cursor+_gem_option_elements)+"]string=="+*global_option );
						
		ELSEIF ( option_type[ cursor ]==_otype_float_arbitrary )	
			
			f_global_option = array_of_pointers[ cursor ];
			text_ids[(cursor+_gem_option_elements)] = write_var( 0, _gem_numbers_x, _gem_line_height*cursor, 0, *f_global_option );
				
			//say( "-text_ids["+(cursor+_gem_option_elements)+"]=="+text_ids[cursor+_gem_option_elements] );
			//say( "-text_ids["+(cursor+_gem_option_elements)+"]string=="+*f_global_option );
						
		END
	END
	
	say( "New_level_menu: text written" );

	cursor = 0;
	x = 160;
	y = _gem_line_height*cursor;
	z = -4;

	
	LOOP

		//COROUTINE CALL
		IF ( current_coroutine_id==0 )
			IF ( option_type[ cursor ]==_otype_uint_arbitrary )
				say( "New_level_menu: option "+cursor+" is int arbitrary" );
				say( "New_level_menu: int_ranges["+cursor+"][0]=="+int_ranges[cursor][0] );
				say( "New_level_menu: int_ranges["+cursor+"][1]=="+int_ranges[cursor][1] );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_int_arbitrary( global_option, &graph, int_ranges[cursor][0], int_ranges[cursor][1] );
				
			ELSEIF ( option_type[ cursor ]==_otype_float_arbitrary )
				say( "New_level_menu: option "+cursor+" is float selector" );
				say( "New_level_menu: float_ranges["+cursor+"][0]=="+float_ranges[cursor][0] );
				say( "New_level_menu: float_ranges["+cursor+"][1]=="+float_ranges[cursor][1] );

				global_option = array_of_pointers[ cursor ];
				current_coroutine_id = Option_float_arbitrary( global_option, &graph, float_ranges[cursor][0], float_ranges[cursor][1] );

			ELSE 
				say( "Init_menu: ERROR could not determine option_type, cursor=="+cursor );
			END
		END
		
		FRAME;

	
		//SET DATA, DEFAULTS OR BACK
		IF ( KEYNEW )  WHILE ( KEYNEW ) FRAME; END
						
			gameplay.ball_j_speed = newgplay.ball_j_speed;
			gameplay.ball_max_i_speed = newgplay.ball_max_i_speed;
			gameplay.paddle_max_i_speed = newgplay.paddle_max_i_speed;
			gameplay.paddle_acc = newgplay.paddle_acc;
			gameplay.paddle_dec = newgplay.paddle_dec;
			gameplay.paddle_convex_nr = newgplay.paddle_convex_nr;
			gameplay.paddle_size = newgplay.paddle_size;
			gameplay.ball_size = newgplay.ball_size;
			gameplay.paddle_type = newgplay.paddle_type;
						
			h_ballsize = gameplay.ball_size/2;
			h_pad_length = gameplay.paddle_size/2;
			
			say( "Gameplay_edit_menu: New gameplay settings set" );
			
			IF ( game_state==2 )  //partida en marcha
				game_state = 3;	
			END
			

		ELSEIF ( key( _d ) )  WHILE ( key( _d ) ) FRAME; END
			
			newgplay.ball_j_speed = _default_ball_j_speed;
			newgplay.ball_max_i_speed = _default_ball_max_i_speed;
			newgplay.paddle_max_i_speed = _default_paddle_max_i_speed;
			newgplay.paddle_acc = _default_paddle_acc;
			newgplay.paddle_dec =_default_paddle_dec;
			newgplay.paddle_convex_nr = _default_paddle_convex_nr;
			newgplay.paddle_size = _default_paddle_size;
			newgplay.ball_size = _default_ball_size;
			newgplay.paddle_type = _default_paddle_type;
					
			say( "Gameplay_edit_menu: Defaults loaded in menu, press N to accept" );

		ELSEIF ( REWIND_K )  WHILE ( REWIND_K ) FRAME; END
			BREAK;
		END
	
		
		//OPTION CHANGE
		IF ( KEYUP OR KEYDOWN )
			IF ( current_coroutine_id!=0 )
				say( "--Gameplay_edit_menu: terminar current_coroutine_id=="+current_coroutine_id );
				signal( current_coroutine_id, s_kill );
				current_coroutine_id = 0;
			END
			
			IF ( KEYUP AND cursor>0 )
				cursor--;
			ELSEIF ( KEYDOWN AND cursor<_gem_option_elements-1 )
				cursor++;
			END
			y = _gem_line_height*cursor;
		END
		
	END

ONEXIT
		
	//text deletion
	
	text_clearer( (_gem_option_elements*2), &text_ids );
	
	/*
	FOR (	cursor = 0; cursor<_gem_option_elements*2; cursor++ )
		IF ( menu_text_id[cursor]!=0 )
					say( "Gameplay_edit_menu: deleting text, menu_text_id["+cursor+"]=="+menu_text_id[cursor] );
			delete_text( menu_text_id[cursor] );
		END
	END
	*/
	
	IF ( current_coroutine_id!=0 )
		signal( current_coroutine_id, s_kill );
		current_coroutine_id = 0;
	END
	IF ( exists( type Menu_cover ) )
		signal( type Menu_cover, S_KILL );
	END

	map_unload( 0, graph );
	
	IF ( game_state==0 )
		Init_menu( );
	END
END