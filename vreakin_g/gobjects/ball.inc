//BALL.INC
//Componentes que rigen el comportamiento de la bola durante la partida
//Ball no se incrusta en bloques, pala ni bordes


//
//PROCESS ball( i_pos, j_pos, float vx, float vy)
//
//DATOS:
//	float vx, float vy    Velocidad de desplazamiento. Se reciben como parámetros
//	int i                 Iterador usado en el cálculo de colisión contra bloques
//	int pcx, int pcy      Guardan la posición en x o y (según colisión) que se encuentra a mitad del tamaño de la bola
//	                        de distancia respecto a la superficie de colisión (según BALLMODE).
//	int shock_id          Guarda la id del proceso contra el que colisiona. Se comprueban los bloques y la pala. Una vez
//	                        obtenida, shock_id se utiliza para acceder a la j_pos de la pala y a los datos necesarios
//	                        para rellenar collision_buffer[] (y undo_buffer[]) para los bloques.
//	int ball_pow          Potencia de impacto
//	int debug_text[2]     Tres campos de texto útiles para mostrar variables
//
// Este proceso representa la bola y se dedica a calcular las colisiones. Puede rebotar de dos maneras, en horizontal y
//en vertical, contra los bordes del tablero, contra la pala y contra los bloques. El valor absoluto de la velocidad vertical
//es constante, mientras que el de la velocidad horizontal sólo cambia al chocar contra la pala. Los rebotes cambian de
//signo el valor del desplazamiento en los ejes que corresponda.
//
// En su bucle principal primero añade vx e vy (desplazamiento) a i_pos y j_pos (posición). Consigue los valores 
//de x,y y comprueba si ha colisionado con un bloque o con la pala. En el caso de haber colisión
//contra un bloque comprueba si es contra más de uno, guardando los valores x, y e id de cada bloque en la casilla 
//pertinente de struct collision_buffer[] . Si el choque es sólo contra un bloque llama a compute_bounce() pasandole parámetros
//desde datos propios y desde el struct collision_buffer[] y modifica vx o vy según el resultado de compute_bounce(). Si el
//choque es contra dos bloques comprueba si estos estan desalineados. En ese caso se cambian tanto vx como vy puesto que el 
//rebote es contra una esquina convexa. Si los dos bloques están alineados comprueba cual está más cerca de la bola y con
//sus datos llama a compute_bounce(). Si el choque es contra tres bloques hace lo mismo que en el caso de dos bloques
//desalineados, cambiar tanto vx como vy. Tras estas comprobaciones envia el mensaje _ball_impact_msg y el valor de ball_pow
//a los bloques pertinentes, guardando estos datos en sus (del bloque) variables locales msg_type y msg_value.  
// Si el choque es contra la pala cambia el signo de vy y recalcula vx. La manera en que lo hace hace que la pala actue como
//si su superfície fuera cóncava. En caso de no haber colisiones comprueba los bordes.
//
// Finalmente, los rebotes se calculan compensando a la salida la penetración de superficies a la entrada. Es decir, si al
//sumar vx o vy a i_pos o j_pos hay una colisión en alguno de los ejes, se reposiciona la pelota a una distancia igual a la de
//penetración en el bloque (cantidad de píxeles que se sobrepondrían) en el eje del rebote en el nuevo sentido que adquiera
//la bola. Para esto se usan pcx y pcy. Si ha habido una colisión se calcula este aspecto del rebote y se vuelve a modificar
//x y/o y. Según el valor de BALLMODE Ball se compila haciendo esta comprobación o no en distintos elementos colisionables.
//(BALLMODE 1=se comprueba en bloques, bordes y pala; 2=no se comprueba; 3=se compureba sólo en bordes y pala).
//
//ATENCION: En ball original cuando el choque era diagonal no se hacia la desincrustacion de PCX ni PCY (supongo que porque no se elegía
//bloque para colisionar)
//
//

PROCESS Ball( i_pos, j_pos, float vx, float vy )

PRIVATE
	int i;
	int pcx;
	int pcy;
	int shock_id;
	int ball_pow = _default_ball_pow;

	int debug_text[2];
END

BEGIN
	say( "Ball: id=="+id );
			
	file = idfpg_rt;
	graph = _ball_graph;
	ball_pos++;
	
	text_z = 0;
	
	debug_text[0] = write( 0, 0, TOTAL_SCREENY-30-35*(ball_pos-1), 0, "bola "+id );
	debug_text[1] = write_var( 0, 0, TOTAL_SCREENY-20-35*(ball_pos-1), 0, vx );
	debug_text[2] = write_var( 0, 0, TOTAL_SCREENY-10-35*(ball_pos-1), 0, vy );

	INCLUDE "sys/ij-to-xy.inc";

	LOOP
		i_pos = i_pos+vx;
		j_pos = j_pos+vy;

		INCLUDE "sys/ij-to-xy.inc";		
	
		IF ( shock_id = collision( type Block ) )	
			REPEAT
				collision_buffer[i].pos_x = shock_id.i_pos;
				collision_buffer[i].pos_y = shock_id.j_pos;
				collision_buffer[i].block_id = shock_id;  //block_id == block_id.id (acceder a la id es tonteria porque collision ya guarda la id en block_id)
				i++;
				shock_id = collision( type block );
			UNTIL ( shock_id ==0 )
				
			say( "////BOUNCE" );
			say( "Ball (id=="+id+"): COLISION contra "+i+" bloques!" );
				
			SWITCH ( i )
				CASE 1:
					say( "Caso 1" );
					IF ( compute_bounce( collision_buffer[0].block_id, i_pos, j_pos, vy, (vy/vx) ) == 1 )
						vx = -vx;
						IF ( collision_buffer[0].pos_x-i_pos > 0 )
							pcx = collision_buffer[0].pos_x-(H_BLOCK_LENGTH+H_BALLSIZE);
						ELSE
							pcx = collision_buffer[0].pos_x+(H_BLOCK_LENGTH+H_BALLSIZE);
						END						
					ELSE
						vy = -vy;
						IF ( collision_buffer[0].pos_y-j_pos > 0 )
							pcy = collision_buffer[0].pos_y-(H_BLOCK_HEIGHT+H_BALLSIZE);
						ELSE
							pcy = collision_buffer[0].pos_y+(H_BLOCK_HEIGHT+H_BALLSIZE);
						END
					END
				END
					
				CASE 2:
					say( "Caso 2" );
					IF ( collision_buffer[0].pos_x != collision_buffer[1].pos_x
					AND  collision_buffer[0].pos_y != collision_buffer[1].pos_y )  //se mira si están desalineados
						vx = -vx;
						vy = -vy;
					ELSE
						IF ( fget_dist( i_pos, j_pos, collision_buffer[0].pos_x, collision_buffer[0].pos_y )
							< fget_dist( i_pos, j_pos, collision_buffer[1].pos_x, collision_buffer[1].pos_y ) )  //comprueba cual es mas cercano
							pcx = 0;  //pcx se usa para guardar un valor temporal que se usará immediatamente
						ELSE
							pcx = 1;
						END

						IF ( compute_bounce( collision_buffer[pcx].block_id, i_pos, j_pos, vy, (vy/vx) ) == 1 )
							vx = -vx;
							IF ( collision_buffer[pcx].pos_x-i_pos > 0 )
								pcx = collision_buffer[pcx].pos_x-(H_BLOCK_LENGTH+H_BALLSIZE);
							ELSE
								pcx = collision_buffer[pcx].pos_x+(H_BLOCK_LENGTH+H_BALLSIZE);
							END								
						ELSE
							vy = -vy;
							IF ( collision_buffer[pcx].pos_y-j_pos > 0 )
								pcy = collision_buffer[pcx].pos_y-(H_BLOCK_HEIGHT+H_BALLSIZE);
							ELSE
								pcy = collision_buffer[pcx].pos_y+(H_BLOCK_HEIGHT+H_BALLSIZE);
							END
							pcx = 0;  //reseteamos su valor porque se uso como variable temporal							
						END
					END
				END
					
				CASE 3:
					say( "Caso 3");
					vx = -vx;
					vy = -vy;
				END
			END
				
			WHILE (i != 0)
				i--;
				collision_buffer[i].block_id.msg_type = _ball_impact_msg;
				collision_buffer[i].block_id.msg_value = ball_pow;
				say( "Ball id "+id+": sending impact message to Block "+collision_buffer[i].block_id );
				say( "--Frame: "+g_frame_counter );
				
				//blocksleft--;
				//signal( collision_buffer[i].block_id, s_kill );
				//say( "Ball (id=="+id+"): Rompiendo bloque collision_buffer["+i+"].block_id=="+collision_buffer[i].block_id+"==>0  i=="+i );
				//undo_buffer[blocksleft].pos_x = collision_buffer[i].pos_x;
				//undo_buffer[blocksleft].pos_y = collision_buffer[i].pos_y;
				//undo_buffer[blocksleft].redo_id = collision_buffer[i].block_id;					
			END

			say( "////BOUNCE_END" );
						
			//redo_limit = blocksleft;
				
		ELSEIF ( shock_id = collision( type Paddle ) )
			//choque contra la pala
			vy = -vy;
			vx = vx+(i_pos-shock_id.i_pos)/gameplay.paddle_convex_nr;
				
			IF ( vx > gameplay.ball_max_i_speed )
				vx = gameplay.ball_max_i_speed;
			ELSEIF ( vx < -gameplay.ball_max_i_speed )
				vx = -gameplay.ball_max_i_speed;
			END
				
			pcy = shock_id.j_pos-H_PAD_HEIGHT-H_BALLSIZE;
		
		ELSE
			//si no choca contra la pala ni contra los bloques
			IF ( i_pos <= H_BALLSIZE )
				vx = -vx;
				pcx = H_BALLSIZE;
			ELSEIF ( i_pos >= level.playfield_i-H_BALLSIZE )
				vx = -vx;
				pcx = level.playfield_i-H_BALLSIZE;
			END
			
			IF ( j_pos < H_BALLSIZE )
				vy = -vy;
				pcy = H_BALLSIZE;
			END
			
			IF ( j_pos > level.playfield_j )
				BREAK;
			END
			
		END

		//RE DISPLAY SI HAY CHOQUE
		IF ( pcy != 0 )
			j_pos = 2*pcy-j_pos;
			pcy = 0;
			IF ( rotacion < 0 )
				x = display_j-j_pos+camera_offset_j;
			ELSE
				y = j_pos-camera_offset_j;
			END
		END
		IF ( pcx != 0 )
			i_pos = 2*pcx-i_pos;
			pcx = 0;
			IF ( rotacion < 0 )
				y = i_pos-camera_offset_i;
			ELSE
				x = i_pos-camera_offset_i;
			END
		END
	
		FRAME;
	END

ONEXIT
	say( "Ball(id=="+id+") saliendo" );
	delete_text( debug_text[0] );
	delete_text( debug_text[1] );
	delete_text( debug_text[2] );
END



//
//FUNCTION compute_bounce( int block, float fi_pos, float fj_pos, float vy, float m )
//
//PRIVATE
//	int block                           //id del bloque a partir del cual se calcula el rebote
//	float fi_pos, float fj_pos          //posición de la bola
//	float vy                            //velocidad vertical de la bola, lo usamos para saber si sube o baja
//	float m                             //pendiente de la recta que forma la dirección de la bola (vx/vy)
//	float xc[1]                         //guardan los puntos de intersección entre la recta de la dirección de la bola y las rectas
//	float yc[1]                             de los bordes del bloque más cercanos a la bola
//	bool pass[1]                        //resultados de pruebas
//
//Esta función es llamada por el proceso Ball() en cuanto colisiona con un proceso Block() y determina el tipo de rebote:
//horizontal o vertical. Para saber esto primero averigua si choca contra un bloque por sus lados o contra su parte superior
//o inferior. Comprueba si la bola chocó por encima o por debajo del bloque así como por que lado lo hizo y se eligen
//dos rectas, una horizontal y otra vertical, en las que están los lados del bloque que está comprobando. Calcula los puntos 
//de intersección entre la recta de la dirección que lleva la bola y las dos rectas que coinciden con los lados del bloque.
//Si el punto de intersección está dentro del segmento que define la cara del bloque, se apunta como prueba pasada. En el
//caso que sólo uno de los dos puntos de intersección cumpla esta condición no hay que hacer más pruebas puesto que el
//sabemos que el rebote será horizontal si la prueba la pasó la recta del lateral del bloque y vertical si fue la otra recta.
//En caso de que los dos puntos de intersección estén comprendidos en el perímetro del bloque se calcula cual de los dos
//es mas cercano al centro de la bola y se elige ese punto como resultado correcto.
//En el caso en que ningún punto de intersección esté comprendido entre los segmentos que definen los lados del bloque (esto
//sucede para algunas direcciones de la bola cuando pasa cerca de los vértices del bloque pero la trayectoria de su centro 
//no entra dentro del area del bloque), el tipo de rebote se decide teniendo en cuenta si la bola está arriba o abajo y si 
//está bajando o subiendo y aplicando un resultado arbitrario (elegido con el criterio de que es lo que quedará mejor en la
//mayoría de los casos)
//
//Devuelve 0 si se da un rebote horizontal y 1 si se da un rebote vertical.
//
//


FUNCTION compute_bounce( int block, float fi_pos, float fj_pos, float vy, float m )

PRIVATE
	float xc[1];
	float yc[1];
	bool pass[1];
END

BEGIN
	say( "compute bounce:" );

	//INTERSECCIÓN A LA HORIZONTAL (REBOTE VERTICAL)
	IF ( fj_pos > block.j_pos )
		yc[0] = block.j_pos+H_BLOCK_HEIGHT;
	ELSE
		yc[0] = block.j_pos-H_BLOCK_HEIGHT;
	END
	
	xc[0] = (yc[0]-(fj_pos-(m)*fi_pos))/(m);  //xc=(yc-b)/m

	IF ( xc[0] >= block.i_pos-H_BLOCK_LENGTH AND xc[0] <= block.i_pos+H_BLOCK_LENGTH )
		pass[0] = 1;
		say( "-INTERSECTA CON HORIZONTAL" );
		say( "--xc[0]=="+xc[0] );
	ELSE
		pass[0] = 0;
		say( "-NO INTERSECTA CON HORIZONTAL" );
		say( "--xc[0]=="+xc[0] );
	END

	//INTERSECCIÓN A LA VERTICAL (REBOTE HORIZONTAL)
	IF ( fi_pos > block.i_pos )
		xc[1] = block.i_pos+H_BLOCK_LENGTH;
	ELSE
		xc[1] = block.i_pos-H_BLOCK_LENGTH;
	END

	yc[1] = (fj_pos-(m)*fi_pos)+(m)*xc[1];  //yc=b+m*xc

	IF (yc[1] >= block.j_pos-H_BLOCK_HEIGHT AND yc[1] <= block.j_pos+H_BLOCK_HEIGHT)
		pass[1] = 1;
		say( "-INTERSECTA CON VERTICAL" );
		say( "--yc[1]=="+yc[1] );
	ELSE
		pass[1] = 0;
		say( "-NO INTERSECTA CON VERTICAL" );
		say( "--yc[1]=="+yc[1] );
	END

	//SELECCIÓN DE INTERSECCIÓN CORRECTA EN CASO DE DOBLE POSITIVO O DOBLE NEGATIVO
	IF ( pass[0] == pass[1] )
		say( "-Selección de intersección correcta" );
		
		IF ( pass[0] == 1 )
			say( "-Doble positivo" );
			say( "--fi_pos=="+(fi_pos*10)+" fj_pos=="+(fj_pos*10)+" c[0] [ "+(xc[0]*10)+", "+(yc[0]*10)+"]  c[1] [ "+(xc[1]*10)+", "+(yc[1]*10)+" ]" );
			//se multiplica por diez para que fget_dist(x0,y0,x1,y1) trabaje en una escala más grande (ya que compara enteros)
					
			IF ( fget_dist( fi_pos*10, fj_pos*10, /*toint*/(xc[0]*10), /*toint*/(yc[0]*10) ) 
				> fget_dist( fi_pos*10, fj_pos*10, /*toint*/(xc[1]*10), /*toint*/(yc[1]*10) ) )
				pass[0] = 0;
			ELSE
				pass[1] = 0;
			END
			
		ELSE
			say( "-Doble negativo" );
			say( "--usando matriz de resultados arbitrarios" );
			
			IF ( fj_pos < block.j_pos )
				IF ( vy > 0 )
					pass[0] = 1;
				ELSE
					pass[1] = 1;
				END
			ELSE
				IF ( vy < 0 )
					pass[0] = 1;
				ELSE
					pass[1] = 1;
				END
			END
			
		END
		
	END

	//REBOTES
	IF ( pass[0] == 1 )
		say( "--COLISION VERTICAL" );

		RETURN ( 0 );
		
	ELSE
		say( "--COLISION HORIZONTAL" );	

		RETURN ( 1 );
		
	END

END