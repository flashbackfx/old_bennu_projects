Build using the BennuGD compiler:

  bgdc ciber-gorilas.prg
or
  bgdc grid-ciber-gorilas.prg

A file with .dcb extension will be created


Run using the BennuGD interpreter:

  bgdi ciber-gorilas.dcb
or
  bgdi grid-ciber-gorilas.dcb


It is the same game, the difference is in how they work internally.